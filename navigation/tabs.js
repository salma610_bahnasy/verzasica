import React from "react";
import {
    Image,
    Text,
    View,
    StyleSheet
} from 'react-native';
import {
    createBottomTabNavigator
} from "@react-navigation/bottom-tabs";
import {
    icons,
    COLORS,
    FONTS
} from "../constants";
import Home from "../screens/Home/Home";
import I18n from "i18n-js";
import Orders from "../screens/Orders/Orders";
import Cart from "../screens/Cart/Cart";
// import ConfirmOrder from "../screens/Order/ConfirmOrder";

import { createStackNavigator } from "@react-navigation/stack";
import Registration from "../screens/Registartion/Registration";
import ConfirmOrder from "../screens/Orders/ConfirmOrder";
import ConfirmCheckOut from "../screens/Cart/ConfirmCheckOut";
import WebView from "../screens/WebView/Purchases";
import Account from "../screens/Account/Account";

const Tab = createBottomTabNavigator();

const tabOptions = {
    showLabel: false,
    style: {
        height: "12%",
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: COLORS.white,
        width: '100%',
        alignSelf: 'center',
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,

    },

}
const ServicesStack = createStackNavigator();

function _ServicesStack() {
    return (
        <ServicesStack.Navigator>
            <ServicesStack.Screen name="Orders" component={Orders} options={{ headerShown: false }} />
            <ServicesStack.Screen name="Registration" component={Registration} options={{ headerShown: false }} />
            <ServicesStack.Screen name="Cart" component={Cart} options={{ headerShown: false }} />
            <ServicesStack.Screen name="ConfirmOrder" component={ConfirmOrder} options={{ headerShown: false }} />
            <ServicesStack.Screen name="ConfirmCheckOut" component={ConfirmCheckOut} options={{ headerShown: false }} />

        </ServicesStack.Navigator>
    );
}

const Tabs = () => {
    return (
        <Tab.Navigator
            tabBarOptions={tabOptions}
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused }) => {
                    const tintColor = focused ? COLORS.black : COLORS.black;

                    switch (route.name) {
                        case "Home":
                            return (
                                <View style={[{
                                    ...styles?.tab,
                                }, focused && {
                                    backgroundColor: COLORS.tabColor,
                                }]}>

                                    {focused &&
                                        <Text style={{ ...styles?.txt }}>
                                            {I18n.t('home')}
                                        </Text>}
                                    <Image
                                        source={icons.home}
                                        resizeMode="contain"
                                        style={{
                                            tintColor: tintColor,
                                            ...styles?.img
                                        }}
                                    />

                                </View>
                            )

                        case "services":
                            return (
                                <View
                                    style={[{
                                        ...styles?.tab,
                                    }, focused && {
                                        backgroundColor: COLORS.tabColor,
                                    }]}
                                >

                                    <Image
                                        source={icons.services}
                                        resizeMode="contain"
                                        style={{
                                            tintColor: tintColor,
                                            ...styles?.img
                                        }}
                                    />
                                    {focused &&
                                        <Text
                                            style={{ ...styles?.txt }}
                                            numberOfLines={1}>
                                            {I18n.t('services')}
                                        </Text>}
                                </View>
                            )
                        case "WebView":
                            return (
                                <View
                                    style={[{
                                        ...styles?.tab,
                                    }, focused && {
                                        backgroundColor: COLORS.tabColor,
                                    }]}
                                >

                                    <Image
                                        source={icons.cart}
                                        resizeMode="contain"
                                        style={{
                                            tintColor: tintColor,
                                            ...styles?.img


                                        }}
                                    />
                                    {focused &&
                                        <Text style={{ ...styles?.txt }}>
                                            {I18n.t('purchases')}
                                        </Text>}
                                </View>
                            )

                        case "account":
                            return (
                                <View
                                    style={[{
                                        ...styles?.tab,
                                    }, focused && {
                                        backgroundColor: COLORS.tabColor,
                                    }]}
                                >
                                    {focused &&
                                        <Text style={{ ...styles?.txt }}>
                                            {I18n.t('myaccount')}
                                        </Text>}
                                    <Image
                                        source={icons?.user}
                                        resizeMode="contain"
                                        style={{
                                            tintColor: tintColor,
                                            ...styles?.img


                                        }}
                                    />

                                </View>
                            )


                    }
                }
            })}
        >
            <Tab.Screen
                name="Home"
                component={Home}
            />
            <Tab.Screen
                name="services"
                component={_ServicesStack}
            />
            <Tab.Screen
                name="WebView"
                component={WebView}
            />
            <Tab.Screen
                name="account"
                component={Account}
            />

        </Tab.Navigator>
    )
}

export default Tabs;


const styles = StyleSheet.create({
    tab: {
        justifyContent: 'center',
        borderColor: COLORS?.secondary,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 20,
        padding: 2,
        paddingHorizontal: 8
    },
    txt: {
        ...FONTS?.body5,
        color: COLORS?.black,
        lineHeight: 35,
        flexShrink: 1,
        marginHorizontal: 3
    },
    img: {
        width: 20,
        height: 20,
    }
})