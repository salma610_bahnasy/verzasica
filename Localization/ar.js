export default {
    greeting: 'Hi!',
    // tabs,
    myaccount: 'حسابي',
    purchases: 'مشتريات',
    services: 'خدمات',
    sreachForServices: 'بحث عن خدمة',
    location: 'الموقع',
    availableTime: 'المواعيد المتاحة',
    orderedService: 'الخدمات المطلوبة',
    available: 'متاح',
    notavailable: 'غير متاح',
    chooseApoi: 'إختيار موعد',
    login: 'تسجيل الدخول',
    signUp: 'مستخدم جديد',
    name: 'الاسم',
    password: 'كلمة المرور',
    phone: 'رقم الجوال',
    email: 'البريد الإلكتروني',
    cpassword: 'تأكيد كلمة المرور',
    record: 'تسجيل',
    enter: 'دخول',
    cart: 'عربية الخدمات',
    qnt: 'الكمية',
    delete: 'حذف',
    continuepayment: 'تابع عملية الدفع',
    rs: 'ريال',
    withvat: 'شامل ضريبة القيمة المضافة',
    total: 'المجموع',
    //tabs:
    home: 'الرئيسية',
    more: 'المزيد',
    to: 'الي',
    confirmOrder: 'تأكيد الطلب',
    Continuetopay: 'متابعة الدفع',
    address: 'العناوين',
    noAddress:'لا يوجد عناوين متاحة',
    addnewAddress: 'إضافة عنوان جديد',
    confirmLocation: 'تأكيد الموقع',
    search: 'بحث عن موقع',
    oneaddress: 'العنوان',
    saveOrders: 'حفظ العنوان',
    changepassword: 'تغيير كلمة المرور',
    language: "اللغة",
    logout: 'تسجيل خروج',
    Favorite: 'المفضلة',
    newpassword: 'كلمة المرور الجديدة',
    confirm: 'تأكيد',
    arabic: 'عربي',
    english: 'انجليزي',
    wallet: 'المحفظة',
    offers: 'العروض',
    walletPrice: 'رصيد المحفظة',
    ourServices: 'خدماتنا',
    ourPurches: 'منتجتنا',
    ourServicesdesc: 'جرب خدماتنا تشعر بالفرق ...',
    ourPurchesdesc: 'تسوق اكثر تربح اكثر ...',
    cart1: 'العربة',
    Notifications: 'الإشعارات',
    changeaddress: "تغيير العنوان",
    paymentWay: 'طرق الدفع',
    wrongcode: 'الكود خاطئ من فضلك حاول مره اخري',
    city: 'المدينة',
    from_where_you_know_us: 'من اين تعرفنا',
    amount: "الكمية المطلوبه",
    totalCart:'مجموع العرية',
    incart:'في عربة التسوق',
    noItems:'لا يوجد خدمات في العربة',
    tryagain:'حاول مره اخري',
    ok:'إلغاء',
    enterYourAddress:'من فضلك ادخل عنوانك',
    quantity:"الكمية",
    rs:'ريال',
    value:'القيمة'















};
