export default {
  greeting: 'Hi!',
  // tabs
  myaccount: 'account',
  purchases: 'purchases',
  services: 'services',
  sreachForServices: 'Sreach For Services',
  location: 'location',
  availableTime: 'Available appointments',
  orderedService: 'Required services',
  available: 'Avialable',
  notavailable: 'Not Avialable',
  chooseApoi:"choose appointments",
  login:"signIn",
  signUp:'signUp',
  name: 'name',
  password: 'Password',
  phone: "Phone",
  email: 'Email ',
  cpassword: 'confirm password',
  record:'Register',
  enter:'Login',
  cart:"cart services",
  qnt:'quantity',
  delete:'delete',
  continuepayment:"Continue Payment",
  rs:'R.S',
  total:'Total',
  withvat:"VAT Included",
  cart1:"Cart",
  //tabs:
  home: 'home',
  more: 'more',
  to: 'to',
  confirmOrder:"Confirm Order",
  Continuetopay:'Continue to pay',
  address:'Address',
  addnewAddress:'Add New Address',
  noAddress:'There Is No Address',

  confirmLocation:'Confirm Location',
  search:'search for location',
  oneaddress:'Address',
  saveOrders: 'Save Orders',
  changepassword: 'change password',
  language: "language",
  logout: 'logout',
  Favorite: 'Favorite',
  newpassword: 'New Password',
  confirm: 'confirm',
  arabic: 'Arabic',
  english: 'English',
  wallet: 'wallet',
  offers: 'offers',
  walletPrice: "walletPrice",
  ourServices: 'Our Services',
  ourPurches: 'Our purchases',
  ourServicesdesc: "Try our services with a difference...",
  ourPurchesdesc: 'Shopping more, earning more...',
  Notifications:"Notifications",
  changeaddress:'Change Address',
  paymentWay:"Payment Way",
  wrongcode:'wrong code please try again',
  city:'City',
  from_where_you_know_us:'from where you know us',
  amount:"Required quantity",
  totalCart:'Total Cart',
  incart:'In Cart',
  noItems:"There are no services in the cart.",
  tryagain:'try again',
  ok:'ok',
  enterYourAddress:"Please Enter Your Address",
  quantity:"quantity",
  rs:'RS',
  value:'value'




};
