import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';

import Tabs from "./navigation/tabs";
import Splash from './screens/Splash';

import Registration from './screens/Registartion/Registration';
import Cart from './screens/Cart/Cart';
import Address from './screens/Address/Address';
import AddAdressByMap from './screens/Address/AddAdressByMap';
import ConfirmAddAddress from './screens/Address/ConfirmAddAddress';
import { Account, Home, Orders, WebView } from './screens';
import ConfirmOrder from './screens/Orders/ConfirmOrder';
import ConfirmCheckOut from './screens/Cart/ConfirmCheckOut';
import Purchases from './screens/WebView/Purchases';
import Favorite from './screens/Favorite/Favorite';
import ChangePassword from './screens/Registartion/ChangePassword';
import Offers from './screens/Offers/Offers';
import Notifications from './screens/Notifications/Notifications';


const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        border: "transparent"
    }
}

const Stack = createStackNavigator();

const App = () => {
    return (
        <NavigationContainer theme={theme}>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false
                }}
                initialRouteName={'Splash'}
            >
                <Stack.Screen name="Splash" component={Splash} />
                {/* Tabs */}
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false }}/>
                <Stack.Screen name="Offers" component={Offers} options={{ headerShown: false }}/>

                {/* Screens */}
                {/* <Stack.Screen name="Registration" component={Registration} options={{ headerShown: false }} /> */}
                <Stack.Screen name="Address" component={Address} options={{ headerShown: false }} />
                <Stack.Screen name="AddAdressByMap" component={AddAdressByMap} options={{ headerShown: false }} />
                <Stack.Screen name="ConfirmAddAddress" component={ConfirmAddAddress} options={{ headerShown: false }} />
                <Stack.Screen name="Orders" component={Orders} options={{ headerShown: false }} />

                <Stack.Screen name="Registration" component={Registration} options={{ headerShown: false }} />
                <Stack.Screen name="Cart" component={Cart} options={{ headerShown: false }} />
                <Stack.Screen name="ConfirmOrder" component={ConfirmOrder} options={{ headerShown: false }} />
                <Stack.Screen name="ConfirmCheckOut" component={ConfirmCheckOut} options={{ headerShown: false }} />

                <Stack.Screen name="Purchases" component={Purchases} options={{ headerShown: false }} />
                <Stack.Screen name="Account" component={Account} options={{ headerShown: false }} />
                <Stack.Screen name="Favorite" component={Favorite} options={{ headerShown: false }} />
                <Stack.Screen name="ChangePassword" component={ChangePassword} options={{ headerShown: false }} />
                <Stack.Screen name="Notifications" component={Notifications} options={{ headerShown: false }}/>


            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default App;