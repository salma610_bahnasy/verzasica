
import {
    StyleSheet,
} from 'react-native';
import { FONTS, COLORS, SIZES } from "../../constants";

const styles = StyleSheet.create({

    mainView: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        marginTop: 20,
        flex: 1,
        width: '95%',
        alignSelf: 'center'
    },
    tabView: {
        width: '100%',
        marginBottom: 30,
        paddingVertical: 10,
        borderRadius: 20,
        borderStyle: 'dashed',
        borderColor: COLORS?.white,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },


    confirmView: {
        backgroundColor: COLORS?.white,
        width: '90%',
        alignSelf: 'center',
        borderRadius: SIZES?.radius,
        height: SIZES?.height * 2 / 3

    },
    whiteTxt: {
        ...FONTS?.h5,
        color: COLORS?.white
    },
    tabArrow: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        tintColor: COLORS?.white,
        resizeMode: 'contain',
        position: 'absolute',
        right: -8
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    locationView: {
        justifyContent: 'space-between',
        marginHorizontal: SIZES.padding15,
        marginTop: SIZES?.Mpading
    },
    smallIcons: {
        width: 20,
        height: 20,
        resizeMode: 'center'
    },
    xSmall: {
        tintColor: COLORS?.gray,
        width: 12,
        height: 12,
        // marginHorizontal: 5
    },
    grayTxt: {
        ...FONTS?.body3,
        color: COLORS?.gray,
        marginHorizontal: SIZES?.smallpading
    },
    servicesView: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        width: '90%',
        backgroundColor: COLORS?.white,
        height: 60,
        marginVertical: SIZES?.padding15,
        alignSelf: 'center',
        borderRadius: SIZES?.radius,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        alignContent: 'center',
        paddingHorizontal: 10
    },
    addServicesBtn: {
        backgroundColor: COLORS?.darkprimary,
        width: 30,
        height: 30,
        borderRadius: 15,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: -20
    },
    countView: {
        backgroundColor: 'red',
        justifyContent: 'space-between',
        paddingHorizontal: 3,
        borderRadius: 10,
        paddingVertical: 1,
        width: 60,
        height: 20

    },
    qtntxt: {
        backgroundColor: COLORS?.white,
        padding: 1,
        borderRadius: 3
    },
    price: {
        backgroundColor: '#ddd',
        borderRadius: 5,
        marginHorizontal: 2,
        paddingHorizontal: 5
    },

})

export default styles