import I18n from "i18n-js";
import React, { useState } from "react";
import {
    SafeAreaView,
    View,
    Text,
    Image,
    TouchableOpacity,
    FlatList,

} from 'react-native';

import {
    COLORS,
    SIZES,
    icons,
    FONTS,

} from '../../constants';
import styles from "./styles";
import MainButton from "../UI/Button";
import MainFooter from "../UI/MainFooter";

/*--------------------------------------------------------*/


const Favorite = ({ navigation }) => {
    const [address, setAddress] = useState([1, 2])


    function _Headers() {
        return (
            <TouchableOpacity
                onPress={() => {
                    navigation.goBack()
                }}
                style={[
                    styles?.row,
                    {
                        justifyContent: 'flex-end',
                        padding: SIZES?.Mpading
                    }]}>
                <Image
                    source={icons?.back_arrow_icon}
                    style={[
                        styles?.smallIcons,
                        {
                            alignSelf: 'flex-end',
                            tintColor: COLORS?.white
                        }
                    ]}
                />
            </TouchableOpacity>
        )
    }

    function _renderAddress() {
        const renderItem = ({ item, index }) => {
            return (
                <View
                    onPress={() => {

                    }}
                    key={`services-${index}`}
                    style={{ ...styles?.servicesView }}

                >
                    <View >
                        <Text style={{ ...FONTS?.h5 }}>
                            نظافة طقم كنب
                        </Text>
                        <View style={{
                            ...styles?.row,
                            justifyContent: 'flex-end'
                        }}>
                            <Text style={{ ...FONTS?.body5 }}>
                                50
                            </Text>
                            <Text style={{ ...FONTS?.body6 }}>
                                ريال
                            </Text>
                        </View>

                      

                    </View>
                    <View
                        style={[
                            styles?.row,
                            styles?.countView
                        ]}>
                        <TouchableOpacity>
                            <Image
                                source={icons?.minus}
                                style={{
                                    ...styles?.xSmall,
                                    tintColor: COLORS?.white
                                }}
                            />
                        </TouchableOpacity>
                        <Text
                            style={[
                                styles?.qtntxt
                            ]}
                        >
                            1
                        </Text>
                        <TouchableOpacity>
                            <Image
                                source={icons?.plus_icon}
                                style={{
                                    ...styles?.xSmall,
                                    tintColor: COLORS?.white
                                }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View >
                        <Text style={{ ...FONTS?.h5 }}>
                            القيمة
                        </Text>
                        <View style={{ ...styles?.row }}>
                            <Text
                                style={styles?.price}>
                                50
                            </Text>
                            <Text
                                style={{ ...FONTS?.body6 }}
                            >
                                الريال
                            </Text>
                        </View>
                    </View>
                </View>
            )
        }
        return (
            <FlatList
                renderItem={renderItem}
                keyExtractor={item => `${item?.id}`}
                data={address}

            />
        )
    }
    // -----------------------------**!--!**--------------------------
 
    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>

            {_Headers()}
            <View
                style={{
                    ...styles?.confirmView,
                }}
            >
                <Text
                    style={{
                        ...FONTS?.h2,
                        textAlign: 'center',
                        marginTop: SIZES?.padding15
                    }}
                >
                    {I18n.t('Favorite')}
                </Text>
                {_renderAddress()}
              
            </View>
            <MainFooter
                focused='account'
            />
        </SafeAreaView>
    )
}

export default Favorite;