import { BaseUrL } from "../../../constants/BaseURL"

const getCurrentUser= (phone) => {
    console.log("phone--->",phone)
    return new Promise((resolve, reject) => {
        console.log(`${BaseUrL}AgentApi/GetByMobile/${phone}`)
        fetch(`${BaseUrL}AgentApi/GetByMobile/${phone}`, {
            method: 'GET',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("getCurrentUser", responseJson)
                resolve(responseJson?.ReturnData)
            })


    })
}

export {
    getCurrentUser
}