
import {

    StyleSheet,
} from 'react-native';
import { FONTS, COLORS, SIZES } from "../../constants";

const styles = StyleSheet.create({
    logo: {
        width: 80,
        height: 50,
        resizeMode: 'contain'
    },
    header: {
        flexDirection: 'row',
        margin: SIZES?.Mpading,
        justifyContent: 'space-between'
    },
    smallIcon: {
        width: 25,
        height: 25,
        tintColor: COLORS?.white,
        marginHorizontal:SIZES.smallpading
    },
    row: {
        flexDirection: 'row',
        alignItems:'flex-end'
    }

})

export default styles