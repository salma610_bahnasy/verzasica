import I18n from "i18n-js";
import React, { useState, useEffect } from "react";
import {
    SafeAreaView,
    View,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';

import {
    COLORS,
    FONTS,
    images,
    SIZES,
    icons
} from '../../constants';
import styles from "./styles";
import FadingSlides from 'react-native-fading-slides';
import MainFooter from "../UI/MainFooter";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { getCurrentUser } from "./Service/MainService";
import Loader from "../UI/Loader";

/*--------------------------------------------------------*/
const slides = [
    {
        image: images?.slider1,
        imageWidth: 200,
        imageHeight: 150,


    },
    {
        image: images?.slide2,
        imageWidth: 200,
        imageHeight: 150,

    },
    {
        image: images?.slide3,
        imageWidth: 300,
        imageHeight: 150,

    }
];

const Home = ({ navigation }) => {
    const [user, setUser] = useState();
    const [WalletBalance, setWalletBalance] = useState(0)
    const [loading, setloading] = useState(true)

    useEffect(() => {
        AsyncStorage.getItem('user').then(res => {
            console.log("user", JSON.parse(res))
            setloading(false)
            if (res != null) {
                setUser(JSON.parse(res))
                console.log(".....", res)
                let parseUser = JSON.parse(res)
                getUser(parseUser?.Mobile)
            }
        })
    }, [])

    const getUser = (phone) => {
        getCurrentUser(phone).then(getCurrentUser => {
            console.log({ getCurrentUser })
            setWalletBalance(getCurrentUser?.WalletBalance)
            AsyncStorage.setItem('cityCode', JSON.stringify(getCurrentUser?.CityCode))
        })
    }


    function HomeMainComp(img, title, description, action) {
        return <TouchableOpacity
            style={{
                width: "45%",
                height: 150,
                marginHorizontal: 10,
                borderRadius: SIZES?.radius,
                marginTop: SIZES?.padding2,
                borderWidth: 1,
                borderColor: COLORS?.white
            }}
            onPress={() => { action() }}
        >
            <Image
                source={img}
                style={{
                    width: '100%',
                    height: 90,
                    borderTopRightRadius: SIZES?.radius,
                    borderTopLeftRadius: SIZES?.radius
                }}
            />
            <View style={{
                backgroundColor: COLORS?.white,
                position: 'absolute',
                bottom: 0,
                width: '100%',
                paddingHorizontal: SIZES?.base
                , borderBottomEndRadius: 10, borderBottomStartRadius: 10,
                paddingVertical: SIZES?.base
            }}>
                <Text style={{
                    ...FONTS?.h3,
                    color: COLORS?.primary
                }}>
                    {title}
                </Text>
                <Text
                    style={{
                        ...FONTS?.body4,
                        color: COLORS?.lightGray3,
                        alignSelf: 'flex-end',
                        fontSize: 12
                    }}
                    numberOfLines={1}
                >
                    {description}
                </Text>
            </View>

        </TouchableOpacity>
    }
    function _Headers() {
        return (
            <View style={{ ...styles?.header }}>
                <Image
                    style={styles?.logo}
                    source={images?.logo}
                />
                {user ? <View
                    style={{ ...styles.row }}
                >

                    <View
                        style={{
                            justifyContent: 'center',
                            alignContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <Text style={{
                            fontFamily: 'Cairo-SemiBold',
                            color: COLORS?.white,
                            marginBottom: SIZES?.smallpading
                        }}>
                            {I18n.t('walletPrice')}
                        </Text>
                        <View
                            style={{
                                borderWidth: 2,
                                borderColor: COLORS?.white,
                                borderRadius: 25,
                                backgroundColor: COLORS?.lightGreen,
                                height: 30,
                                width: 80
                            }}
                        >
                            <Text style={{
                                // ...FONTS?.h4,
                                color: COLORS?.darkRed,
                                textAlign: 'center',
                                fontFamily: 'Cairo-Bold',
                            }}

                            >
                                {WalletBalance}
                            </Text>
                            <Text style={{
                                fontFamily: 'Cairo-SemiBold',
                                color: COLORS?.white,
                                position: 'absolute',
                                right: 8,
                                bottom: 0,
                                fontSize: 8
                                // ...FONTS?.h5
                            }}>
                                {I18n.t('rs')}
                            </Text>
                        </View>

                    </View>
                    <TouchableOpacity
                        onPress={() => {
                            navigation.navigate('Notifications')
                        }}
                    >
                        <Image
                            style={{ ...styles.smallIcon }}
                            source={icons.notification_icon}
                        />
                    </TouchableOpacity>

                </View>
                    : null}
            </View>
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>
            {loading == true ?
                <Loader
                    color={COLORS?.white}
                    size={"large"}
                /> :
                <>
                    {_Headers()}
                    <View style={{
                        backgroundColor: COLORS?.white,
                        width: '100%',
                        height: 150
                    }}>
                        <FadingSlides
                            slides={slides}
                            fadeDuration={1200}
                            stillDuration={1000}
                            height={150}
                        // startAnimation={true}
                        />
                    </View>
                    <View
                        style={{ flexDirection: 'row', paddingTop: SIZES?.padding20 }}
                    >
                        {HomeMainComp(images?.slider1,
                            I18n.t('ourServices'),
                            I18n.t('ourServicesdesc'),
                            () => { navigation.navigate('Orders') }
                        )}
                        {HomeMainComp(images?.products,
                            I18n.t('ourPurches'),
                            I18n.t('ourPurchesdesc'),
                            () => { navigation.navigate('Purchases') }
                        )}

                    </View>
                    <MainFooter
                        focused='home'
                    />
                </>}
        </SafeAreaView>
    )
}

export default Home;