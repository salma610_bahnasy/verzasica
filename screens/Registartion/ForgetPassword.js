import React from "react";
import {
    View,
    Text,
    ImageBackground,
    TouchableOpacity,
    Image,
    I18nManager,
    SafeAreaView
} from 'react-native';
import { COLORS, images } from "../../constants";
import I18n from 'react-native-i18n';
import en from '../../Localization/en';
import ar from '../../Localization/ar';
import MainButton from "../UI/Button";
import MainTextInput from "../UI/TextInput";
import styles from "./styles";
import MainHeader from "../UI/MainHeader";

I18n.fallbacks = true;

I18n.translations = {
    en,
    ar
};

const ForgetPassword = ({ route, navigation }) => {
    const [phone, setPhone] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [showPassword, setShowPassword] = React.useState(false)

    return (
        <SafeAreaView>
            <ImageBackground
                style={styles.bg}
                source={images?.bg}
            >
                <View style={styles.Card}>
                    <MainHeader
                        iconColor={COLORS?.primary_500}
                        textStyle={{color:COLORS?.primary_500}}
                        title={I18n.t('forgetyourpassword')}
                        icon={I18nManager.isRTL ? 'chevron-right' : 'chevron-left'}
                        goBack={() => { navigation.goBack() }} />
                    <Image
                        style={styles.images}
                        source={I18nManager.isRTL ? images?.logoAr : images?.logoEn} />
                    <Text style={styles.txt}>
                        {I18n.t('forgetyourpassworddes')}
                    </Text>
                    <Text style={styles.subtxt}>
                        {I18n.t('writephone')}
                    </Text>
                    <View style={styles.form}>
                        <MainTextInput
                            type='phone'
                            title={'phonePlaceHolder'}
                            icon={'mobile-alt'}
                            onChangeText={(val) => { setPhone(val) }}
                            value={phone}
                            textstyle={{ textAlign: 'center' }}

                        />

                        <MainButton
                            title={I18n.t('confirm')}
                            color={COLORS?.secondary}
                            titleColor={COLORS?.white}
                        />
                    </View>

                </View>
            </ImageBackground>
        </SafeAreaView>
    )

}

export default ForgetPassword;

