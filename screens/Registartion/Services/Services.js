import { BaseUrL } from "../../../constants/BaseURL"


const Login = (phone) => {
    return new Promise((resolve, reject) => {
        fetch(`${BaseUrL}AuthApi/Login/${phone}`, {
            method: 'GET',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("getLiveTripPPPP", responseJson)
                resolve(responseJson)
            })


    })
};

const GetAgentByMobile = (phone) => {
    return new Promise((resolve, reject) => {
        fetch(`${BaseUrL}AgentApi/GetByMobile/${phone}`, {
            method: 'GET',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("GetAgentByMobile", responseJson)
                resolve(responseJson)
            })


    })
};

const GetCities= () => {
    return new Promise((resolve, reject) => {
        fetch(`${BaseUrL}CityApi/GetCity`, {
            method: 'GET',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("GetAgentByMobile", responseJson)
                resolve(responseJson?.ReturnData)
            })


    })
};
const GetChanels= () => {
    return new Promise((resolve, reject) => {
        fetch(`${BaseUrL}ChannelApi/GetChannel`, {
            method: 'GET',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("GetAgentByMobile", responseJson)
                resolve(responseJson?.ReturnData)
            })


    })
};
const Register = (phone) => {
    return new Promise((resolve, reject) => {
        fetch(`${BaseUrL}AuthApi/Register/${phone}`, {
            method: 'GET',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("GetAgentByMobile", responseJson)
                resolve(responseJson)
            })


    })
};
const CreateAgent = (name, phone, email,city,chanel) => {
    let data = {
        "AgentName": name,
        "Mobile": phone,
        "Email": email,
        "CityCode": city,
        "ChannelCode": chanel,
        "AgentAddStatusCode": 1
    }
    console.log('data',data)
    return new Promise((resolve, reject) => {
        fetch(`${BaseUrL}AgentApi/Create`, {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json',
            },
            body: JSON.stringify(data)
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("CreateAgent", responseJson)
                resolve(responseJson)
            })


    })
};
export {
    Login,
    GetAgentByMobile,
    Register,
    CreateAgent,
    GetChanels,
    GetCities
}