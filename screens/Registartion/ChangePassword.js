import I18n from "i18n-js";
import React, { useState } from "react";
import {
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    Image,
    FlatList,
    I18nManager,
    TextInput
} from 'react-native';

import {
    COLORS,
    SIZES,
    icons,
    FONTS,
    images
} from '../../constants';
import styles from "./styles";
import LinearGradient from 'react-native-linear-gradient';
import CalendarPicker from 'react-native-calendar-picker';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import AppointmentsModal from "../UI/AppointmentsModal";
import MainTextInput from "../UI/TextInput";
import MainButton from "../UI/Button";
import MainFooter from "../UI/MainFooter";


/*--------------------------------------------------------*/


const ChangePassword = ({ navigation }) => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [password, setPassword] = useState('')
    const [cpassword, setCPassword] = useState('')
    const [isloading, setIloading] = useState(false)



    function _Headers() {
        return (
            <TouchableOpacity
                onPress={() => {
                    navigation.goBack()
                }}
                style={[
                    styles?.row,
                    {
                        justifyContent: 'flex-end',
                        paddingHorizontal: SIZES?.Mpading, marginVertical: SIZES?.base
                    }]}>
                <Image
                    source={
                        I18nManager.isRTL?icons?.back_arrow_icon:icons?.right_arrow_icon
                    }
                    style={[
                        styles?.smallIcons,
                        {
                            alignSelf: 'flex-end',
                            tintColor:COLORS?.white
                        }
                    ]}
                />
            </TouchableOpacity>
        )
    }

  

    function _loginView() {
        return <View style={{ marginTop: SIZES?.padding20 }}>
            <MainTextInput
                title={I18n.t('password')}
                type={'password'}
                onChangeText={(val) => { setPhone(val) }}
            />
            <MainTextInput
                title={I18n.t('newpassword')}
                type={'password'}
                onChangeText={(val) => { setPassword(val) }}
            />
            <MainTextInput
                title={I18n.t('cpassword')}
                type={'password'}
                onChangeText={(val) => { setPassword(val) }}
            />
            <MainButton
                title={I18n.t('confirm')}
                color={isloading ? COLORS?.lightGray3 : COLORS?.primary}
                titleColor={COLORS?.white}
                BtnAction={() => {
                    navigation.navigate('Cart')
                }}
            />
        </View>
    }
    
    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>

            {_Headers()}
            <View
                style={{
                    ...styles?.confirmView,
                    height: SIZES?.height * 0.5 

                }}
            >
                <Text
                    style={{
                        ...FONTS?.h2,
                        textAlign: 'center',
                        marginTop: SIZES?.padding15
                    }}
                >
                    {I18n.t('changepassword')}
                </Text>
                    {_loginView()}
                 
            </View>
            <MainFooter

                focused='account'
            />
        </SafeAreaView>
    )
}

export default ChangePassword;