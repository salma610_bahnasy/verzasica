import React from "react";
import {
    View,
    Text,
    ImageBackground,
    TouchableOpacity,
    Image,
    I18nManager,
    SafeAreaView
} from 'react-native';
import {COLORS, images } from "../../constants";
import I18n from 'react-native-i18n';
import en from '../../Localization/en';
import ar from '../../Localization/ar';
import MainButton from "../UI/Button";
import MainTextInput from "../UI/TextInput";
import styles from "./styles";

I18n.fallbacks = true;

I18n.translations = {
    en,
    ar
};

const SignUp = ({ route, navigation }) => {
    const [phone, setPhone] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [showPassword, setShowPassword] = React.useState(false)

    return (
        <SafeAreaView>
            <ImageBackground
                style={styles.bg}
                source={images?.bg}
            >
                <View style={styles.Card}>
                    <Image
                        style={styles.images}
                        source={I18nManager.isRTL ? images?.logoAr : images?.logoEn} />
                        <Text style={styles.txt}>
                                {I18n.t('activeAccountDesic')}
                        </Text>
                        <Text style={styles.subtxt}>
                                {I18n.t('writephone')}
                        </Text>
                    <View style={styles.form}>
                        <MainTextInput
                            type='phone'
                            title={'phonePlaceHolder'}
                            icon={'mobile-alt'}
                            onChangeText={(val) => { setPhone(val) }}
                            value={phone}
                            textstyle={{textAlign:'center'}}

                        />
                        
                        <MainButton
                            title={I18n.t('ActiveAccount')}
                            color={COLORS?.secondary}
                            titleColor={COLORS?.white}
                        />
                    </View>
                    <TouchableOpacity
                        onPress={()=>{navigation.navigate('Login')}}
                        style={{ marginBottom: 20 }}>
                        <Text style={styles.txt}>{I18n.t('login')}</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </SafeAreaView>
    )

}

export default SignUp;

