import I18n from "i18n-js";
import React, { useState, useRef, useEffect } from "react";
import {
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    Image,
    FlatList,
    I18nManager,
    TextInput
} from 'react-native';

import {
    COLORS,
    SIZES,
    icons,
    FONTS,
    images
} from '../../constants';
import styles from "./styles";
import LinearGradient from 'react-native-linear-gradient';
import CalendarPicker from 'react-native-calendar-picker';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import AppointmentsModal from "../UI/AppointmentsModal";
import MainTextInput from "../UI/TextInput";
import MainButton from "../UI/Button";
import MainFooter from "../UI/MainFooter";
import { CreateAgent, GetAgentByMobile, GetChanels, GetCities, Login, Register } from "./Services/Services";
import AsyncStorage from "@react-native-async-storage/async-storage";
import CheckCode from "../UI/CheckCode";
import DropDownPicker from 'react-native-dropdown-picker';


/*--------------------------------------------------------*/


const Registration = ({ navigation, route }) => {
    const [tab, setTab] = useState('login')
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [phoneError, setPhoneError] = useState(false)
    const [nameError, setNameError] = useState(false)
    const [emailError, setEmailError] = useState(false)

    const [showVcode, setShowVcode] = useState(false)
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([]);

    const [ChanelOpen, setChanelOpen] = useState(false);
    const [chanelValue, setChanelValue] = useState(null);
    const [chanelItems, setChanelItems] = useState([]);

    const [loading, setLoading] = useState(false)
    const [rightCode, setRightCode] = useState(false)
    const [message, setmassage] = useState('')

    useEffect(() => {
        _getChanel()
        _getCities()
    },[])


    const _login = () => {
        if (phone == '') {
            setPhoneError(true)
        } else {
            setLoading(true)
            Login(phone).then(res => {
                console.log("..res..", res)
                setLoading(false)
                if (res.Message.Type == "Success") {
                    setShowVcode(true)
                    setRightCode(res?.ReturnData)
                }
            })
        }
    }
    const ـregister = () => {
        if (name == '') {
            setNameError(true)
        } else if (phone == '') {
            setPhoneError(true)
        } else if (email == '') {
            setEmailError(true)
        } else {
            setLoading(true)
            Register(phone).then(res => {
                console.log("..res..", res)
                setLoading(false)
                if (res.Message.Type == "Success") {
                    setShowVcode(true)
                    setRightCode(res?.ReturnData)
                }
            })
        }
    }
    const _createAagent = (code) => {
        setLoading(true)
        if (code == rightCode) {
            CreateAgent(name, phone, email,value,chanelValue).then(res => {
                console.log("....", res)
                setShowVcode(false)
                setLoading(false)
                if (res.Message.Type == "Success") {
                    AsyncStorage.setItem('user', JSON.stringify(res?.ReturnData), () => {
                        navigation.navigate('Home')
                    })
                } else {
                    setmassage(I18nManager.isRTL ?
                        res?.Message?.ContentAr :
                        res?.Message?.ContentEr)
                }
            })
        } else {
            setmassage(I18n.t('wrongcode'))
            setLoading(false)

        }
    }
    const _checkVcode = (code) => {

        if (code == '' || code.length != 4) {
            setmassage(I18n.t('wrongcode'))
        } else {
            if (code == rightCode) {
                GetAgentByMobile(phone).then(res => {
                    console.log(res)
                    setShowVcode(false)
                    if (res?.ReturnData) {
                        AsyncStorage.setItem('user', JSON.stringify(res?.ReturnData), () => {
                            if (route?.params?.footer) {
                                navigation.navigate('Cart')
                            } else {
                                navigation.navigate('Home')
                            }
                        })
                    } else {
                        setmassage(I18nManager.isRTL ?
                            res?.Message?.ContentAr :
                            res?.Message?.ContentEr)
                    }

                })
            } else {
                setmassage(I18n.t('wrongcode'))
            }
        }
    }
    const _getCities = () => {
        GetCities().then(res => {
            console.log(res)
            let arr = []
            for (let i = 0; i < res?.length; i++) {
                let label = res[i]?.CityName
                let value = res[i]?.CityCode
                arr[i] = { 'label': label, 'value': value }
                console.log("arr...[]", arr)

            }
            if (arr?.length > 0) {
                setItems(arr)
            }

        })
    }
    const _getChanel = () => {


        GetChanels().then(res => {
            console.log(res)
            let arr = []
            for (let i = 0; i < res?.length; i++) {
                let label = res[i]?.ChannelName
                let value = res[i]?.ChannelCode
                arr[i] = { 'label': label, 'value': value }
                console.log("arr...[]", arr)

            }
            if (arr?.length > 0) {
                setChanelItems(arr)
            }


        })


    }

    function _Headers() {
        return (
            <View
                style={[
                    styles?.row,
                    { justifyContent: 'flex-end', paddingHorizontal: SIZES?.Mpading }]}>
                <Image
                    source={images?.logo}
                    style={[
                        styles?.smallIcons,
                        {
                            alignSelf: 'flex-end'
                        }
                    ]}
                />
            </View>
        )
    }

    function _TabsView() {
        return (<View
            style={
                styles?.RowTabView
            }
        >
            <TouchableOpacity
                onPress={() => {
                    setTab('login')
                }}
                style={{
                    ...styles?.tabBtn,
                    borderBottomWidth: tab == "login" ? 2 : 0,
                    borderColor: COLORS?.primary
                }}
            >
                <Text style={{
                    ...FONTS?.h4,
                    color: tab == "login" ?
                        COLORS?.black :
                        COLORS?.lightGray3

                }}>
                    {I18n.t('login')}
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => {
                    setTab('signUp')
                }}
                style={{
                    ...styles?.tabBtn,
                    borderBottomWidth: tab == "signUp" ? 2 : 0,
                    borderColor: COLORS?.primary
                }}
            >
                <Text style={{
                    ...FONTS?.h4,
                    color: tab == "signUp" ?
                        COLORS?.black :
                        COLORS?.lightGray3
                }}>
                    {I18n.t('signUp')}

                </Text>
            </TouchableOpacity>

        </View>)

    }

    function _loginView() {
        return <View style={{ marginTop: SIZES?.padding20 }}>
            <MainTextInput
                title={I18n.t('phone')}
                type={'phone'}
                onChangeText={(val) => {
                    setPhoneError(false)
                    setPhone(val)
                }}
                error={phoneError}
            />

            <MainButton
                loading={loading}
                title={I18n.t('enter')}
                color={loading ? COLORS?.lightGray3 : COLORS?.primary}
                titleColor={COLORS?.white}
                BtnAction={() => {
                    _login()
                }}
            />
        </View>
    }
    function _signUpView() {
        return <View style={{ marginTop: SIZES?.padding20 }}>
            <MainTextInput
                title={I18n.t('name')}
                type={'name'}
                onChangeText={(val) => { setName(val) }}
                error={phoneError}

            />
            <MainTextInput
                title={I18n.t('phone')}
                type={'phone'}
                onChangeText={(val) => { setPhone(val) }}
                error={nameError}

            />
            <MainTextInput
                title={I18n.t('email')}
                type={'email'}
                onChangeText={(val) => { setEmail(val) }}
                error={emailError}

            />

            <DropDownPicker
                open={open}
                value={value}
                items={items}
                setOpen={setOpen}
                setValue={setValue}
                setItems={setItems}
                // multiple={true}
                placeholder={I18n.t('city')}
                textStyle={{
                    ...FONTS?.body5
                }}
                onChangeValue={(value) => {
                    console.log(value);
                }}
                style={{
                    // width: '85%',
                    borderRadius: SIZES?.padding,
                    backgroundColor: COLORS?.lightGray2,
                    borderColor: COLORS?.lightGray2,
                    marginVertical: SIZES?.base


                }}
                containerStyle={{
                    width: '90%',
                    alignSelf: 'center',
                    borderRadius: SIZES?.padding
                }}
                zIndex={60000}


            />
            <DropDownPicker
                open={ChanelOpen}
                value={chanelValue}
                items={chanelItems}
                setOpen={setChanelOpen}
                setValue={setChanelValue}
                setItems={setChanelItems}
                // multiple={true}
                placeholder={I18n.t('from_where_you_know_us')}
                textStyle={{
                    ...FONTS?.body5
                }}
                onChangeValue={(value) => {
                    console.log(value);
                }}
                style={{
                    // width: '85%',
                    borderRadius: SIZES?.padding,
                    backgroundColor: COLORS?.lightGray2,
                    borderColor: COLORS?.lightGray2,
                    marginVertical: SIZES?.base

                }}
                containerStyle={{
                    width: '90%',
                    alignSelf: 'center',
                    borderRadius: SIZES?.padding
                }}


            />
            <MainButton
                title={I18n.t('record')}
                color={loading ? COLORS?.lightGray3 : COLORS?.primary}
                titleColor={COLORS?.white}
                BtnAction={() => {
                    tab != 'login' && ـregister()
                }}
            />
        </View>
    }
    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>
            <CheckCode
                isVisible={showVcode}
                loading={loading}
                checkCode={(code) => {
                    tab != 'login' ?
                        _createAagent(code)
                        : _checkVcode(code)
                }}
                Dismiss={() => {
                    setShowVcode(false)
                }}
                message={message}
            />
            {_Headers()}
            <View
                style={{
                    ...styles?.confirmView,
                    height: tab == 'login' ? SIZES?.height * 0.5 : SIZES?.height * 3 / 4

                }}
            >
                {_TabsView()}
                {tab == 'login' ?
                    _loginView() :
                    _signUpView()}
            </View>
            {route?.params?.footer && <MainFooter
                focused='order'
            />}
        </SafeAreaView>
    )
}

export default Registration;