import React from "react";
import {
    View,
    Text,
    ImageBackground,
    TouchableOpacity,
    Image,
    I18nManager,
    SafeAreaView
} from 'react-native';
import { COLORS, FONTS, images } from "../../constants";
import I18n from 'react-native-i18n';
import en from '../../Localization/en';
import ar from '../../Localization/ar';
import MainButton from "../UI/Button";
import MainTextInput from "../UI/TextInput";
import styles from "./styles";
import { CheckBox } from "react-native-elements";

I18n.fallbacks = true;

I18n.translations = {
    en,
    ar
};

const Login = ({ route, navigation }) => {
    const [phone, setPhone] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [showPassword, setShowPassword] = React.useState(false)
    const [remeberME, setRemberMe] = React.useState(false)

    return (
        <SafeAreaView>
            <ImageBackground
                style={styles.bg}
                source={images?.bg}
            >
                <View style={styles.Card}>
                    <Image
                        style={styles.images}
                        source={I18nManager.isRTL ? images?.logoAr : images?.logoEn} />
                    <View style={styles.form}>
                        <MainTextInput
                            type='phone'
                            title={'loginhomePlaceholder'}
                            icon={'mobile-alt'}
                            onChangeText={(val) => { setPhone(val) }}
                            value={phone}

                        />
                        <MainTextInput
                            type='password'
                            title={'password'}
                            icon={'lock'}
                            onChangeText={(val) => { setPassword(val) }}
                            setShowPassword={(val) => { setShowPassword(val) }}
                            showPassword={showPassword}
                            value={password}

                        />
                        <View style={styles.remeberMEView}>
                            <View style={styles.checkbox}>
                                <CheckBox
                                    checked={remeberME}
                                    onPress={() => {
                                        setRemberMe(!remeberME)
                                    }}
                                />
                                <Text style={{...FONTS.h5,color:COLORS?.primary}}>
                                    {I18n.t('remeberme')}
                                </Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => {
                                    navigation.navigate('ForgetPassword')
                                }}
                            >

                                <Text style={{...FONTS.h5,color:COLORS?.primary}}>
                                    {I18n.t('forgetyourpassword')}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <MainButton
                            title={I18n.t('login')}
                            color={COLORS?.secondary}
                            titleColor={COLORS?.white}
                            BtnAction={()=>{navigation?.navigate('Home')}}
                        />
                    </View>
                    <TouchableOpacity
                        onPress={() => { navigation.navigate('SignUp') }}
                        style={{ marginBottom: 20 }}>
                        <Text style={styles.txt}>{I18n.t('ActiveAccount')}</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        </SafeAreaView>
    )

}

export default Login;

