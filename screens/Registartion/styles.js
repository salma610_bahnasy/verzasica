
import {
    StyleSheet, I18nManager
} from 'react-native';
import { FONTS, COLORS, SIZES } from "../../constants";

const styles = StyleSheet.create({
    mainView: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        marginTop: 20,
        flex: 1,
        width: '95%',
        alignSelf: 'center'
    },
    tabView: {
        width: '100%',
        marginBottom: 40,
        paddingVertical: 10,
        borderRadius: 20,
        borderStyle: 'dashed',
        borderColor: COLORS?.white,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    whiteTxt: {
        ...FONTS?.h5,
        color: COLORS?.white
    },
    tabArrow: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        tintColor: COLORS?.white,
        resizeMode: 'contain',
        position: 'absolute',
        right: -8
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    locationView: {
        justifyContent: 'space-between',
        marginHorizontal: SIZES.padding15,
        marginTop: SIZES?.Mpading
    },
    smallIcons: {
        width: 40,
        height: 40,
        resizeMode: 'contain'
    },
    xSmall: {
        tintColor: COLORS?.gray,
        width: 15,
        height: 15,
        // marginHorizontal: 5
    },
    grayTxt: {
        ...FONTS?.body3,
        color: COLORS?.gray,
        marginHorizontal: SIZES?.smallpading
    },
    error:{
        ...FONTS?.body5,
        color: COLORS?.secondary,
        marginHorizontal: SIZES?.smallpading,
        textAlign:'center'
    },
    servicesView: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        width: '90%',
        backgroundColor: COLORS?.white,
        height: 60,
        marginVertical: SIZES?.padding15,
        alignSelf: 'center',
        borderRadius: SIZES?.radius,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        alignContent: 'center',
        paddingHorizontal: 10
    },
    addServicesBtn: {
        backgroundColor: COLORS?.darkprimary,
        width: 30,
        height: 30,
        borderRadius: 15,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: -20
    },
    countView: {
        backgroundColor: 'red',
        justifyContent: 'space-between',
        paddingHorizontal: 5,
        borderRadius: 10,
        paddingVertical: 1,
        width: 60

    },
    qtntxt: {
        backgroundColor: COLORS?.white,
        padding: 1,
        borderRadius: 3
    },
    price: {
        backgroundColor: '#ddd',
        borderRadius: 5,
        marginHorizontal: 2,
        paddingHorizontal: 5
    },
    // confirm order
    RowTabView: {
        width: '90%',
        alignSelf: 'center',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: SIZES?.padding20
    },
    confirmView: {
        backgroundColor: COLORS?.white,
        width: '90%',
        alignSelf: 'center',
        borderRadius: SIZES?.radius,
    },
    tabBtn: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        flex: 1,
        padding: SIZES?.smallpading
    },
    indicator: {
        borderWidth: 1,
        borderStyle: 'dotted',
        borderRadius: 5,
        borderColor: COLORS?.primary,
        marginHorizontal: SIZES?.base,
        width: 20, height: 20,
    },
    day: {
        width: 40,
        borderWidth: 1.5,
        borderStyle: 'dotted',
        borderRadius: 10, padding: 5,
        borderColor: COLORS?.primary,
    },
    vcodeView: {
        flexDirection: I18nManager?.isRTL ? 'row-reverse' : 'row',
        alignItems: 'center',
        width: '90%',
        alignSelf: 'center'
    },
    vcode: {
        flex: 1,
        borderWidth: .5,
        borderColor: COLORS?.primary,
        justifyContent: 'center',
        marginHorizontal: SIZES?.smallpading,
        borderRadius: SIZES?.radius
    },
    inputvcode: {
        textAlign: 'center'
    }
})

export default styles