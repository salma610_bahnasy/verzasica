import I18n from "i18n-js";
import React, { useState } from "react";
import { useRef } from "react";
import {
    SafeAreaView,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import WebView from "react-native-webview";

import {
    COLORS,
    SIZES,
    icons,
    FONTS,

} from '../../constants';
import MainFooter from "../UI/MainFooter";
import styles from "./styles";

/*--------------------------------------------------------*/


const Purchases = ({ navigation }) => {
    const MyWebView=useRef()


    function _Headers() {
        return (
            <TouchableOpacity
                onPress={() => {
                    navigation.goBack()
                }}
                style={[
                    styles?.row,
                    {
                        justifyContent: 'flex-end',
                        padding: SIZES?.Mpading
                    }]}>
                <Image
                    source={icons?.back_arrow_icon}
                    style={[
                        styles?.smallIcons,
                        {
                            alignSelf: 'flex-end',
                            tintColor: COLORS?.white
                        }
                    ]}
                />
            </TouchableOpacity>
        )
    }



    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>

            {/* {_Headers()}
            <View
                style={{
                    ...styles?.confirmView,
                }}
            >
                <Text
                    style={{
                        ...FONTS?.h2,
                        textAlign: 'center',
                        marginTop: SIZES?.padding15
                    }}
                >
                    {I18n.t('address')}
                </Text>
            </View> */}
            <WebView
                originWhitelist={['*']}
                style={{
                    backgroundColor:COLORS?.primary
                }}
                injectedJavaScript={'if(window.myLoad)window.myLoad();true;'}
                onMessage={event => {
                    const { data } = event.nativeEvent;
                    // setPicture(data);
                }}
                ref={MyWebView}
                source={{ uri: "https://salla.sa/verzasca/" }} />
            
            
            <MainFooter
                focused='purchases'
            />

        </SafeAreaView>
    )
}

export default Purchases;