import React, { useRef, useEffect } from "react";
import {
    View,
    Text,
    Image,
    I18nManager,
    StyleSheet,
    Easing,
    Animated,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import I18n from 'react-native-i18n';
import AsyncStorage from "@react-native-async-storage/async-storage";
import RNRestart from 'react-native-restart'; // Import package from node modules
import { COLORS, images, SIZES } from "../constants";
import en from '../Localization/en';
import ar from '../Localization/ar';

I18n.fallbacks = true;
I18n.translations = {
    en,
    ar
};
const Splash = ({ route, navigation }) => {
    const animatedValue = useRef(new Animated.Value(1)).current  // Initial value for opacity: 0

    useEffect(() => {
        Animated.timing(animatedValue, {
            toValue: 0,
            duration: 2000,
            easing: Easing.ease,
            useNativeDriver: false
        }).start()
    }, [])
    useEffect(() => {
        SplashActions()
    }, [])
    const SplashActions = () => {
        AsyncStorage.getItem('language').then(lang => {
            console.log("lacalization", lang)
            if (lang != null) {
                I18n.locale = lang;
                I18nManager.forceRTL(lang == 'ar' ? true : false)
                I18nManager.allowRTL(lang == 'ar' ? true : false)
                redirect()

            } else {
                AsyncStorage.setItem('language', 'ar')
                I18n.locale = 'ar';
                I18nManager.forceRTL(true)
                I18nManager.allowRTL(true)
                RNRestart.Restart();


            }
        })


    }
    const redirect = () => {
        setTimeout(() => {
            AsyncStorage.getItem('token').then(token => {
                console.log('token', token)
                if (token != null) {
                    navigation.navigate('Home')

                } else {
                    navigation.navigate('Home')
                }
            })
        }, 3000);
    }
    return (<TouchableOpacity style={styles?.SplashScreen}>
        <ImageBackground
            source={images?.splash}
            style={{
                ...styles.bg
            }}>
            <Animated.Image
                source={images?.logo}
                useNativeDriver={false}
                style={{
                    ...styles.logo,
                    transform: [

                        {
                            scaleX: animatedValue.interpolate({
                                inputRange: [0, 1],
                                outputRange: [1, 2]
                            })
                        },
                        {
                            scaleY: animatedValue.interpolate({
                                inputRange: [0, 1],
                                outputRange: [1, 2]
                            })
                        }
                    ]
                }}
            />
        </ImageBackground>
    </TouchableOpacity>
    )

}

export default Splash;

const styles = StyleSheet.create({
    SplashScreen: {
        backgroundColor: COLORS?.primary,
        width: SIZES.width,
        height: SIZES.height,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },
    bg: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'

    },
    logo: {
        width: 150,
        height: 150,
        resizeMode: 'contain'
    }

})