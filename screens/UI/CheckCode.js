import React,{useRef} from "react";
import {
    Text,
    StyleSheet,
    View,
    TextInput,
    I18nManager
} from 'react-native';
import {
    COLORS,
    FONTS,
    SIZES
} from "../../constants";
import Modal from "react-native-modal";
import { useNavigation } from "@react-navigation/native";
import MainButton from "./Button";
import I18n from "i18n-js";


const CheckCode = (props) => {
    const [show, setShow] = React.useState(false)
    const navigation = useNavigation()
    const [code1, setcode1] = React.useState('')
    const [code2, setcode2] = React.useState('')
    const [code3, setcode3] = React.useState('')
    const [code4, setcode4] = React.useState('')
    const ref_input2 = useRef();
    const ref_input3 = useRef();
    const ref_input4 = useRef();



    return (
        <View style={{ backgroundColor: '#000' }}>
            <Modal
                isVisible={props?.isVisible}
                useNativeDriver={true}
                onDismiss={() => {
                    setShow(false)
                    props?.Dismiss(false)
                }}
                onBackdropPress={() => {
                    setShow(false)
                    props?.Dismiss(false)
                }}
                // animationIn='slideInUp'
                // animationOut='slideOutDown'
                style={
                    styles.Modal
                }

            >
                <View style={{backgroundColor:'#fff',padding:SIZES?.padding,borderRadius:SIZES?.radius}}>
                    <View style={{ ...styles?.vcodeView }}>
                        <View style={{ ...styles?.vcode }}>
                            <TextInput
                                placeholder="*"
                                style={{ ...styles?.inputvcode }}
                                maxLength={1}
                                onChangeText={(val) => {
                                    setcode1(val)
                                    if (val && val.length == 1) {
                                        ref_input2.current.focus()
                                    }
                                }}
                                onSubmitEditing={() => {
                                    ref_input2.current.focus()
                                }}
                                returnKeyType={'next'}
                                autoFocus={true}
                                keyboardType='numeric'
                                value={code1}
                            />
                        </View>
                        <View style={{ ...styles?.vcode }}>
                            <TextInput
                                placeholder="*"
                                style={{ ...styles?.inputvcode }}
                                maxLength={1}
                                onChangeText={(val) => {
                                    setcode2(val)
                                    if (val && val.length == 1) {
                                        ref_input3.current.focus()
                                    }
                                }}
                                returnKeyType={"next"}
                                onSubmitEditing={() => ref_input3.current.focus()}
                                ref={ref_input2}
                                keyboardType='numeric'
                                value={code2}
                            />
                        </View>
                        <View style={{ ...styles?.vcode }}>
                            <TextInput
                                placeholder="*"
                                style={{ ...styles?.inputvcode }}
                                maxLength={1}
                                onChangeText={(val) => {
                                    setcode3(val)
                                    if (val && val.length == 1) {
                                        ref_input4.current.focus()
                                    }
                                }}
                                onSubmitEditing={() => ref_input4.current.focus()}
                                ref={ref_input3}
                                keyboardType='numeric'
                                value={code3}

                            />
                        </View>
                        <View style={{ ...styles?.vcode }}>
                            <TextInput
                                placeholder="*"
                                style={{ ...styles?.inputvcode }}
                                maxLength={1}
                                onChangeText={(val) => {
                                    setcode4(val)
                                }}
                                returnKeyType={"done"}
                                ref={ref_input4}
                                keyboardType='numeric'
                                value={code4}
                            />
                        </View>
                    </View>
                    <Text style={{ ...styles?.error }}>{props?.message}</Text>
                    <MainButton
                        loading={props?.loading}
                        title={I18n.t('confirm')}
                        color={props?.loading ? COLORS?.lightGray3 : COLORS?.primary}
                        titleColor={COLORS?.white}
                        BtnAction={() => {
                            let code = ''
                            code = code1 + code2 + code3 + code4
                            props?.checkCode(code)
                        }}
                    />
                </View>
            </Modal>
        </View>
    )

}

export default CheckCode;

const styles = StyleSheet.create({

    Modal: {
        width: SIZES?.width,
        height: SIZES?.height,
        alignSelf: 'center',
        justifyContent: 'flex-start'

    },
    vcodeView: {
        flexDirection: I18nManager?.isRTL ? 'row-reverse' : 'row',
        alignItems: 'center',
        width: '90%',
        alignSelf: 'center'
    },
    vcode: {
        flex: 1,
        borderWidth: .5,
        borderColor: COLORS?.primary,
        justifyContent: 'center',
        marginHorizontal: SIZES?.smallpading,
        borderRadius: SIZES?.radius
    },
    inputvcode: {
        textAlign: 'center'
    },
    error:{
        ...FONTS?.body5,
        color: COLORS?.secondary,
        marginHorizontal: SIZES?.smallpading,
        textAlign:'center'
    },
})