import React from "react";
import {

    ActivityIndicator
} from 'react-native';
import {

    SIZES
} from "../../constants";


const Loader = (props) => {

    return (
        <ActivityIndicator
            animating={true}
            size={props?.size}
            style={{ opacity: 1, marginHorizontal: SIZES?.smallpading }}
            color={props?.color}
        />

    )

}

export default Loader;

