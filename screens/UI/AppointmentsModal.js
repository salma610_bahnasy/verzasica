import React,
{
    useEffect,
    useRef,
    useState
} from "react";
import {
    Text,
    StyleSheet,
    View,
    FlatList,
    TouchableOpacity,
    ScrollView,
    SafeAreaView
} from 'react-native';
import { COLORS, FONTS, SIZES } from "../../constants";
import I18n from 'react-native-i18n';
import RBSheet from "react-native-raw-bottom-sheet";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { saveOrder } from "../Orders/Services/Services";
import { getCartItmsNum } from "../../constants/Global";
import FeedBackMsg from "./FeedBackMsg";


const AppointmentsModal = (props) => {
    const refRBSheet = useRef();
    console.log("props?.isVisiable", props?.isVisiable)
    const [user, setUser] = useState(null)
    const [feedBackMessage, setFeedBackMessage] = useState('')

    const navigation = useNavigation()

    useEffect(() => {
        if (props?.isVisiable == true) {
            refRBSheet.current.open()
        } else {
            refRBSheet.current.close()
            props?.onDismiss(false)
        }
    }, [props])

    useEffect(() => {
        AsyncStorage.getItem('user').then(res => {
            setUser(JSON.parse(res))
        })
       
    }, [])

    function _renderTimesView() {
        const renderItem = ({ item, index }) => {
            return (
                <TouchableOpacity
                    key={`times-:${index}`}
                    style={{
                        ...styles?.btn,
                        ...styles?.shadow,
                        marginLeft: index % 2 == 0 ? 10 : 0
                    }}
                    onPress={() => {
                        props?.saveOrder(item?.WorkTimeCode)
                        refRBSheet.current.close()
                        props?.onDismiss(false)


                    }}
                >
                    <Text
                        style={{
                            ...FONTS?.h2,
                            color: COLORS?.gray,
                            lineHeight: 40
                        }}
                    >{item?.WorkTime}</Text>
                    {/* <Text
                        style={{
                            ...FONTS?.body3,
                            color: COLORS?.white,
                            marginLeft: 5
                        }}
                    >صباحاَ</Text> */}
                </TouchableOpacity>
            )
        }
        return (
            <View style={{ flex: 1, marginBottom: 50 }}>
                <FlatList
                    numColumns={2}
                    renderItem={renderItem}
                    // keyExtractor={item => `${item}`}
                    data={props?.timeObj}

                />
            </View>
        )
    }
    return (
       
            <RBSheet
                ref={refRBSheet}
                closeOnPressMask={true}
                customStyles={{
                    wrapper: {
                        backgroundColor: "transparent",

                    },
                    draggableIcon: {
                        backgroundColor: "#000"
                    },
                    container: {
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        width: '95%',
                        alignSelf: 'center',
                        ...styles?.shadow
                    }
                }}
                height={300}


            >

                <ScrollView
                    style={{
                        ...styles?.container,
                    }}
                >
                    <Text
                        style={
                            styles?.centertitle
                        }>
                        {I18n.t('chooseApoi')}
                    </Text>
                    <View
                        style={
                            styles?.line
                        }
                    />
                    {_renderTimesView()}

                </ScrollView>
            </RBSheet>

    )

}

export default AppointmentsModal;

const styles = StyleSheet.create({
    line: {
        borderWidth: 3,
        borderColor: COLORS?.lightGray4,
        width: 80,
        alignSelf: 'center',
        marginBottom: 20,
        borderRadius: 10
    },
    centertitle: {
        ...FONTS?.h2,
        color: COLORS?.primary,
        textAlign: 'center',
    },
    container: {
        backgroundColor: COLORS?.white,
        width: SIZES?.width,
        height: SIZES?.height,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingTop: SIZES?.padding
    },
    btn: {
        backgroundColor: COLORS?.primary,
        borderRadius: SIZES.base,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'flex-start',
        marginVertical: SIZES?.base,
        flex: 1,
        marginRight: 30,
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }

})