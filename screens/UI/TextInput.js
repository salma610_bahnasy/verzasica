import React from "react";
import {
    Text, StyleSheet, TextInput, View, Image
} from 'react-native';
import { COLORS, FONTS, SIZES, icons } from "../../constants";
import { Icon } from 'react-native-elements'
import I18n from 'react-native-i18n';


const MainTextInput = (props) => {

    return (
        <View style={[styles.inputContainer,props?.error&&{borderWidth:1,borderColor:COLORS?.secondary}]}>
            {props.icon && <Icon
                name={props.icon}
                type='font-awesome-5'
                style={styles.icon}
                color={COLORS?.primary_500}
                size={18} />}
            <TextInput
                style={[styles.input, { ...props?.textstyle }]}
                onChangeText={(val) => {
                    props.onChangeText(val)
                }}
                placeholder={props?.title}
                secureTextEntry={props.type == "password" ?
                    props?.value?.length > 0 &&
                        props?.showPassword == false ?
                        true : false
                    : false}
                keyboardType={props.type == "phone" ? 'numeric' : 'default'}

            />
            {
                props?.righticon &&
                <Image
                    style={{
                        width: 20,
                        height: 20,
                        resizeMode: 'contain'
                    }}
                    source={props?.righticon}
                />
            }
            {/* {props.type == "password" && <Icon
                onPress={() => {
                    props.setShowPassword(!props?.showPassword)
                }}
                name={
                    props?.showPassword
                        ? 'eye-slash' : 'eye'
                }
                type='font-awesome-5'
                style={styles.icon} color={COLORS?.primary_500} size={18} />} */}
        </View>
    )

}

export default MainTextInput;

const styles = StyleSheet.create({
    inputContainer: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: SIZES?.padding,
        paddingHorizontal: 10,
        width: '90%',
        marginVertical: 10,
        backgroundColor: COLORS?.lightGray2

    },
    input: {
        // height: 50,
        flex: 1,
        ...FONTS?.body4,
        textAlign: 'right',
        paddingHorizontal: SIZES.padding

    },
    icon: {
        // position: 'absolute',
        // right: 10,zIndex:10
        color: COLORS?.secondary,
        marginRight: 10,
    }
})