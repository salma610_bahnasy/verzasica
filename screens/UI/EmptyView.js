import React from "react";
import {
    Text, StyleSheet, TouchableOpacity, View, Image
} from 'react-native';
import { COLORS, FONTS, SIZES } from "../../constants";
import Loader from "./Loader";


const EmptyView = (props) => {


    return (
        <View style={{
            ...styles?.container
        }}>
            <Image
                style={{
                    ...styles?.img,
                    ...props?.imgstyle
                }}
                source={props?.img}
            />
            <Text style={{
                ...styles?.txt,
                ...props?.txtstyle
            }}>
                {props?.title}
            </Text>
        </View>
    )

}

export default EmptyView;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        marginTop: SIZES?.padding
    },
    img: {
        width: 80,
        height: 80,
        alignSelf: 'center'
    },
    txt: {
        ...FONTS?.h4,
        marginTop: SIZES.Mpading

    }
})