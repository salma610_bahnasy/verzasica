import React, { useRef } from "react";
import {
    Text,
    StyleSheet,
    View,
    TextInput,
    I18nManager
} from 'react-native';
import {
    COLORS,
    FONTS,
    SIZES
} from "../../constants";
import Modal from "react-native-modal";
import { useNavigation } from "@react-navigation/native";
import MainButton from "./Button";
import I18n from "i18n-js";
import Loader from "./Loader";


const OverLoader = (props) => {
    const [show, setShow] = React.useState(false)
    const navigation = useNavigation()




    return (
        <View style={{ backgroundColor: '#000' }}>
            <Modal
                isVisible={props?.isVisible}
                useNativeDriver={true}
                onDismiss={() => {
                    setShow(false)
                    props?.Dismiss(false)
                }}
                onBackdropPress={() => {
                    setShow(false)
                    props?.Dismiss(false)
                }}
                // animationIn='slideInUp'
                // animationOut='slideOutDown'
                style={
                    styles.Modal
                }

            >
                <View
                    style={{
                        ...styles?.mainView
                    }}
                >
                    <Loader color={COLORS?.primary} />
                </View>

            </Modal>
        </View>
    )

}

export default OverLoader;

const styles = StyleSheet.create({

    Modal: {
        width: SIZES?.width,
        height: SIZES?.height,
        alignSelf: 'center',
        justifyContent: 'flex-start'

    },
    mainView: {
        width: '100%',
        height: '100%',
        justifyContent:'center'
    }
})