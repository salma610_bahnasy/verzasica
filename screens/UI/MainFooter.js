import React, { useEffect, useState } from "react";
import {
    Text,
    StyleSheet,
    TouchableOpacity,
    View,
    Image,
} from 'react-native';
import {
    COLORS,
    FONTS,
    icons,
    SIZES
} from "../../constants";
import { useNavigation } from "@react-navigation/native";
import I18n from "i18n-js";
import AsyncStorage from "@react-native-async-storage/async-storage";


const MainFooter = (props) => {
    const navigation = useNavigation()
    const focused = props?.focused
    const tintColor = COLORS?.black
    const [user, setUser] = useState(null)

    useEffect(() => {
        AsyncStorage.getItem('user').then(user => {
            console.log({ user })
            setUser(user)
        })
    },[])
    return (
        <View
            style={{ ...styles.footer }}
        >
            <TouchableOpacity
                onPress={() => {
                    navigation.navigate('Home')
                }}
                style={[{
                    ...styles?.tab,
                }, focused == 'home' && {
                    backgroundColor: COLORS.tabColor,
                }]}>
                <Image
                    source={icons.home}
                    resizeMode="contain"
                    style={{
                        tintColor: tintColor,
                        ...styles?.img
                    }}
                />

                <Text style={{ ...styles?.txt }}>
                    {I18n.t('home')}
                </Text>


            </TouchableOpacity>
            <TouchableOpacity
                style={[{
                    ...styles?.tab,
                }, focused == 'order' && {
                    backgroundColor: COLORS.tabColor,
                }]}

                onPress={() => {
                    if (user == null) {
                        navigation.navigate('Registration', {
                            footer: false
                        })

                    } else {
                        navigation.navigate('Cart')
                    }
                }}
            >

                <Image
                    source={icons.wallet}
                    resizeMode="contain"
                    style={{
                        tintColor: tintColor,
                        ...styles?.img
                    }}
                />

                <Text
                    style={{ ...styles?.txt }}
                    numberOfLines={1}>
                    {I18n.t('cart1')}
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={[{
                    ...styles?.tab,
                }, focused == 'Offers' && {
                    backgroundColor: COLORS.tabColor,
                }]}

                onPress={() => {
                    navigation.navigate('Offers')
                }}
            >

                <Image
                    source={icons.offers}
                    resizeMode="contain"
                    style={{
                        tintColor: tintColor,
                        ...styles?.img


                    }}
                />

                <Text style={{ ...styles?.txt }}>
                    {I18n.t('offers')}
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={[{
                    ...styles?.tab,
                }, focused == 'account' && {
                    backgroundColor: COLORS.tabColor,
                }]}

                onPress={() => {
                    if (user == null) {
                        navigation.navigate('Registration', {
                            footer: false
                        })

                    } else {
                        navigation.navigate('Account')
                    }
                }}
            >
                <Image
                    source={icons?.user}
                    resizeMode="contain"
                    style={{
                        tintColor: tintColor,
                        ...styles?.img


                    }}
                />

                <Text style={{ ...styles?.txt }}>
                    {I18n.t('myaccount')}
                </Text>


            </TouchableOpacity>
        </View>
            
    )

}

export default MainFooter;

const styles = StyleSheet.create({
    footer: {

        flexDirection: 'row',
        alignSelf: 'center',
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40,
        position: "absolute",
        bottom: 0,
        backgroundColor: COLORS?.white,
        width: SIZES?.width,
        // height: 80,
        paddingHorizontal: SIZES?.base,
        paddingVertical: SIZES?.radius,
        // paddingTop:30

    },
    tab: {
        justifyContent: 'center',
        borderColor: COLORS?.secondary,
        // flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 20,

        paddingHorizontal: 8,
        flex: 1,
        paddingVertical: 5
    },
    txt: {
        color: COLORS?.black,
        flexShrink: 1,
        fontFamily: 'Cairo-SemiBold'
        // marginHorizontal: 3
    },
    img: {
        width: 20,
        height: 18,
    }
})