import React from "react";
import {
    Text,
    StyleSheet,
    TouchableOpacity,
    View,
    Image,
} from 'react-native';
import {
    COLORS,
    FONTS,
    icons,
    SIZES
} from "../../constants";
import Modal from "react-native-modal";
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from "@react-navigation/native";
import I18n from "i18n-js";


const SelectCity = (props) => {
    const [show, setShow] = React.useState(false)
    const navigation = useNavigation()

    function _renderproducts() {
        return (
            <View style={{
                ...styles?.row,


            }}>
                <View style={{ flex: 3 }}>
                    <View style={[styles?.row]}>
                        <Image
                            style={{
                                ...styles?.xSmall,
                                tintColor: COLORS?.lightGray
                            }}
                            source={icons?.addressIcon}
                        />
                        <Text
                            style={{
                                ...FONTS?.h4, marginHorizontal: 5
                            }}
                        >
                           {I18n.t('enterYourAddress')}
                        </Text>
                    </View>
                   
                </View>
             

            </View>
        )
    }

    return (
        <View style={{ backgroundColor: '#000' }}>
            <Modal
                isVisible={props?.isVisible}
                useNativeDriver={true}
                onDismiss={() => {
                    setShow(false)
                    props?.Dismiss(false)
                }}
                onBackdropPress={() => {
                    setShow(false)
                    props?.Dismiss(false)
                }}
                animationIn='slideInUp'
                animationOut='slideOutDown'
                style={
                    styles.Modal
                }

            >
                <View style={{
                    width: '90%',
                    alignSelf: 'center',
                    backgroundColor: COLORS?.white,
                    height: 150,
                    justifyContent: 'space-between',
                    paddingHorizontal: SIZES?.Mpading,
                    paddingVertical: 40
                }}>
                    {_renderproducts()}
                    <View
                        style={{
                            ...styles?.row_space,
                        }}>


                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            colors={['#536c68', '#21ba9f']}
                            style={{
                                ...styles?.btn
                            }}>

                            <TouchableOpacity
                                onPress={() => {
                                    props?.Dismiss(false)
                                    navigation.navigate('Address')
                                }}
                                style={{
                                    width: '100%', 
                                    alignContent:'center',
                                    alignItems:'center'
                                }}
                            >
                                <Text
                                    style={{
                                        ...FONTS?.body5,
                                        color: COLORS?.white,
                                        textTransform:'capitalize' 
                                    }}
                                >
                                   {I18n.t('confirm')}
                            </Text>
                            </TouchableOpacity>
                        </LinearGradient>
                        <TouchableOpacity
                            style={{
                                ...styles?.btn
                            }}
                            onPress={()=>{props?.Dismiss(false)}}
                            >

                            <Text
                                style={{ ...FONTS?.body5,textTransform:'capitalize' }}

                            >
                               {I18n.t('ok')}
                            </Text>
                        </TouchableOpacity>


                    </View>
                </View>
            </Modal>
        </View>
    )

}

export default SelectCity;

const styles = StyleSheet.create({

    Modal: {
        width: SIZES?.width,
        height: SIZES?.height,
        alignSelf: 'center',
        justifyContent: 'flex-start'

    },
    title: {
        ...FONTS?.h3,
        color: COLORS?.white
    },
    subtitle: {
        ...FONTS?.h4,
        color: COLORS?.white
    },
    row_space: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    xSmall: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    btn: {
        borderWidth: 1,
        borderColor: COLORS?.primary,
        borderRadius: SIZES?.padding15,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        paddingHorizontal: SIZES?.padding15,
        flex: 1,
        marginHorizontal: SIZES?.Mpading,
        paddingVertical: SIZES.smallpading
    }
})