import React from "react";
import {
    Text,
    StyleSheet,
    TouchableOpacity,
    View,
    Image,
    ImageBackground,
    SafeAreaView
} from 'react-native';
import { COLORS, FONTS, icons, SIZES } from "../../constants";
import Modal from "react-native-modal";


const Zoom = (props) => {
    const [show, setShow] = React.useState(false)


    return (
        <View style={{ backgroundColor: '#000' }}>
            <Modal
                isVisible={props?.isVisible}
                useNativeDriver={true}
                onDismiss={() => {
                    setShow(false)
                    props?.Dismiss(false)
                }}
                onBackdropPress={() => {
                    setShow(false)
                    props?.Dismiss(false)
                }}
                animationIn='slideInUp'
                animationOut='slideOutDown'
                transparent={false}
                style={
                    styles.Modal
                }

            // coverScreen={true}
            >

                <ImageBackground
                    source={props?.Image}
                    style={styles.imagbg}
                    resizeMode={'contain'}
                >
                    <TouchableOpacity
                        onPress={() => {
                            setShow(false)
                            props?.Dismiss(false)
                        }}
                        style={{ height: 50 }}>
                        <Image
                            source={icons?.close}
                            style={styles.closeIcon}
                        />
                    </TouchableOpacity>
                    <View style={{ flex: 1 }} />
                    <View style={{
                        width: '100%',
                        backgroundColor: COLORS?.primary_opacity
                    }}>
                        <ImageBackground
                            source={props?.Image}
                            style={{
                                width: SIZES?.width,
                                alignSelf: 'center'
                            }}
                            blurRadius={5}
                        >
                            <View style={{
                                width: '100%',
                                backgroundColor: COLORS?.primary_opacity,
                                padding: SIZES?.Mpading,

                            }}>
                                <View style={
                                    styles.row_space
                                }>
                                    <View style={styles.row}>
                                        <Image
                                            source={icons?.message}
                                            style={styles?.closeIcon}
                                        />
                                        <Text
                                            numberOfLines={1}
                                            style={styles?.title}>
                                            تفاصيل الخبر عنوان الصوره مسار الجدول
                                        </Text>

                                    </View>
                                    <TouchableOpacity
                                        onPress={() => {
                                            setShow(!show)
                                        }}
                                    >
                                        <Image
                                            source={show ? icons?.down_arrow : icons?.up_arrow}
                                            style={styles?.closeIcon}
                                        />
                                    </TouchableOpacity>
                                </View>
                                {show &&
                                    <View style={
                                        styles.row_space
                                    }>
                                        <View style={styles.row}>
                                            <Image
                                                source={icons?.message}
                                                style={styles?.closeIcon}
                                            />
                                            <Text
                                                numberOfLines={1}
                                                style={styles?.subtitle}>
                                                تفاصيل الخبر عنوان الصوره مسار الجدول
                                            </Text>

                                        </View>
                                        <View
                                            style={styles.row}
                                        >
                                            <Image
                                                source={icons?.download}
                                                style={styles?.closeIcon}
                                            />
                                            <Image
                                                source={icons?.share}
                                                style={styles?.closeIcon}
                                            />
                                        </View>
                                    </View>
                                }
                            </View>
                        </ImageBackground>
                    </View>
                </ImageBackground>
            </Modal>
        </View>
    )

}

export default Zoom;

const styles = StyleSheet.create({
    closeIcon: {
        tintColor: COLORS?.white,
        width: 20,
        height: 20,
        alignSelf: 'flex-end',
        marginVertical: SIZES.Mpading,
        marginHorizontal: SIZES.Mpading
    },
    imagbg: {
        width: '100%',
        height: '100%',
        alignSelf: 'center'
    },
    Modal: {
        width: SIZES?.width,
        height: SIZES?.height,
        alignSelf: 'center',
        // paddingHorizontal: SIZES?.Mpading,
        // paddingVertical: SIZES?.padding

    },
    title: {
        ...FONTS?.h3,
        color: COLORS?.white
    },
    subtitle: {
        ...FONTS?.h4,
        color: COLORS?.white
    },
    row_space: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    }
})