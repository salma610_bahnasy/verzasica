import React from "react";
import {
    Text, StyleSheet, TouchableOpacity
} from 'react-native';
import { COLORS, FONTS, SIZES } from "../../constants";
import Loader from "./Loader";


const MainButton = (props) => {


    return (
        <TouchableOpacity
            style={[styles.btn, { backgroundColor: props?.color, ...props?.style }]}
            onPress={() => { props?.BtnAction() }}
        >
            {props?.loading ?
                <Loader color={COLORS?.white} />
                :
                <Text style={[styles.txt, { color: props?.titleColor }]}>{props?.title}</Text>}
        </TouchableOpacity>
    )

}

export default MainButton;

const styles = StyleSheet.create({
    btn: {
        width: '90%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        padding: 10,
        borderRadius: SIZES?.padding,
        marginVertical: 20

    },
    txt: {
        ...FONTS?.h4,

    }
})