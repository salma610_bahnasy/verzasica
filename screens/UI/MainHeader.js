import React from "react";
import {
    Text, StyleSheet, TouchableOpacity, View
} from 'react-native';
import { Icon } from "react-native-elements";
import { FONTS, SIZES } from "../../constants";


const MainHeader = (props) => {


    return (
        <View style={styles.row}>
            <TouchableOpacity
                style={{ marginHorizontal: 10 }}
                onPress={() => { props?.goBack() }}>
                <Icon
                    name={props?.icon}
                    type='font-awesome-5'
                    size={18}
                    color={props?.iconColor}
                />
            </TouchableOpacity>
            <Text style={[styles.txt, { ...props?.textStyle }]}>{props?.title}</Text>
        </View>
    )

}

export default MainHeader;

const styles = StyleSheet.create({
    btn: {
        width: '90%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        padding: 10,
        borderRadius: 10,
        marginVertical: 20

    },
    txt: {
        ...FONTS?.h5,

    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        marginVertical:SIZES?.Mpading
    }
})