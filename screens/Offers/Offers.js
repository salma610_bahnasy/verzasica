import I18n from "i18n-js";
import React, { useState } from "react";
import {
    SafeAreaView,
    View,
    Text,
    Image,
    TouchableOpacity,
    FlatList,

} from 'react-native';

import {
    COLORS,
    SIZES,
    icons,
    FONTS,
    images
} from '../../constants';
import styles from "./styles";
import MainButton from "../UI/Button";
import MainFooter from "../UI/MainFooter";

/*--------------------------------------------------------*/


const Offers = ({ navigation }) => {
    const [address, setAddress] = useState([1, 2])




    function _renderAddress() {
        const renderItem = ({ item, index }) => {
            return (
                <View
                    onPress={() => {

                    }}
                    key={`services-${index}`}
                    style={{ ...styles?.servicesView }}

                >
                    <View>
                        <Image
                        source={images.products}
                        style={{...styles.image}}
                        
                        />
                    </View>
                    <View style={{marginHorizontal:SIZES?.smallpading}} >
                        <Text style={{ ...FONTS?.h5 }}>
                            احصل علي خصم 50% عند شراء بقيمه 500 ريال
                        </Text>
                        <Text style={{ ...FONTS?.body6 }}>
                           ( شقه كامله - كنب - تنضيف)
                        </Text>



                    </View>

                
                </View>
            )
        }
        return (
            <FlatList
                renderItem={renderItem}
                keyExtractor={item => `${item?.id}`}
                data={address}

            />
        )
    }
    // -----------------------------**!--!**--------------------------

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>

         
            <Text
                style={{
                    ...FONTS?.h2,
                    textAlign: 'center',
                    color: COLORS?.white,
                    marginVertical:SIZES?.padding
                }}
            >
                {I18n.t('offers')}
            </Text>
            {_renderAddress()}

            <MainFooter
                focused='Offers'
            />
        </SafeAreaView>
    )
}

export default Offers;