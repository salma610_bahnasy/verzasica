import { BaseUrL } from "../../../constants/BaseURL"
import Geolocation from '@react-native-community/geolocation';
import { PERMISSIONS, requestMultiple } from 'react-native-permissions';
import { Platform } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

const getAllAddress = (agentCode) => {
    return new Promise((resolve, reject) => {
        console.log(`${BaseUrL}AgentLocationApi/GetByAgentCode/${agentCode}`)
        fetch(`${BaseUrL}AgentLocationApi/GetByAgentCode/${agentCode}`, {
            method: 'GET',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("getCurrentUser", responseJson)
                resolve(responseJson?.ReturnData)
            })


    })
}
const handel_permissioms = () => {
    return new Promise((resolve, reject) => {
        if (Platform.OS = 'android') {
            requestMultiple([
                PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
                PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION]).then(
                    (statuses) => {
                        console.log("statuses", statuses)
                        resolve(true)
                    },
                );

        } else {
            console.log("Permissions.request")
            requestMultiple([
                PERMISSIONS.IOS.LOCATION_ALWAYS,
                PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
            ]).then(
                (statuses) => {
                    console.log("statuses ios", statuses)
                    resolve(true)
                },
            );
        }
    })
}
const getCurrentPosition = async () => {
    return new Promise((resolve, reject) => {
        Geolocation.getCurrentPosition(
            position => {
                const initialPosition = JSON.stringify(position);
                console.log(initialPosition);
                resolve(position)
            },
            error => Alert.alert('Error', JSON.stringify(error)),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    })
}

//post
const addAddress = (CityCode, Address, lat, lng) => {

    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('user').then(res => {
            console.log(JSON.parse(res))
            let user = JSON.parse(res)
            let data = {
                "AgentCode": user?.AgentCode,
                CityCode,
                Address,
                lat,
                lng
            }
            console.log({ data })
            fetch(`${BaseUrL}AgentLocationApi/Create`, {
                method: 'POST',
                headers: {
                    "Content-Type": 'application/json',
                },
                body: JSON.stringify(data)
            }).then((response) => response.json())
                .then((responseJson) => {
                    console.log("addAddress", responseJson)
                    resolve(responseJson)
                })

        })
    })
}
//delete
const Deleteddress = (id) => {

    return new Promise((resolve, reject) => {
        console.log(`${BaseUrL}AgentLocationApi/Delete/${id}`)
        fetch(`${BaseUrL}AgentLocationApi/Delete/${id}`, {
            method: 'DELETE',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("Deleteddress", responseJson)
                resolve(responseJson)
            })

    })
}
export {
    getAllAddress,
    handel_permissioms,
    getCurrentPosition,
    addAddress,
    Deleteddress

}