
import {
    StyleSheet,
} from 'react-native';
import { FONTS, COLORS, SIZES } from "../../constants";

const styles = StyleSheet.create({

    bottom: {
        position: 'absolute',
        bottom: 80,
        width: '100%'
    },
    top: {
        position: 'absolute',
        top:50,
        width: '100%',
        zIndex:500
    },
    container: {
        ...StyleSheet.absoluteFillObject,
        height: SIZES.height,
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },

    whiteTxt: {
        ...FONTS?.h5,
        color: COLORS?.white
    },

    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    smallIcons: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    xSmall: {
        tintColor: COLORS?.gray,
        width: 15,
        height: 15,
        // marginHorizontal: 5
    },
    grayTxt: {
        ...FONTS?.body6,
        color: COLORS?.gray,
        marginHorizontal: SIZES?.smallpading
    },

    confirmView: {
        backgroundColor: COLORS?.white,
        width: '90%',
        alignSelf: 'center',
        borderRadius: SIZES?.radius,
        height: SIZES?.height -100

    },
    title: {
        ...FONTS?.h4,
        color: COLORS?.primary,
        marginHorizontal: SIZES?.smallpading
    },
    addressView:{
        paddingVertical:SIZES?.base,
       
        borderRadius:SIZES?.radius,
        width:'95%',
        alignSelf:'center',
        backgroundColor:COLORS?.white

    }

})

export default styles