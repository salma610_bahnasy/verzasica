import I18n from "i18n-js";
import React, { useEffect, useState } from "react";
import {
    SafeAreaView,
    View,
    Text,
    Image,
    TouchableOpacity,
    TextInput,
    I18nManager
} from 'react-native';

import {
    COLORS,
    SIZES,
    icons,
    FONTS,

} from '../../constants';
import styles from "./styles";
import DropDownPicker from 'react-native-dropdown-picker';
import MainTextInput from "../UI/TextInput";
import MainButton from "../UI/Button";
import MainFooter from "../UI/MainFooter";
import { addAddress } from "./services/AddressServices";
import { useRoute } from "@react-navigation/native";
import { GetCities } from "../Registartion/Services/Services";
import Loader from "../UI/Loader";
import FeedBackMsg from "../UI/FeedBackMsg";
/*--------------------------------------------------------*/


const ConfirmAddAddress = ({ navigation }) => {
    const route = useRoute()
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([]);
    const [isloading, setIsloading] = useState(true);
    const [error, seterror] = useState(false);
    const [feedBackMessage, setFeedBackMessage] = useState('');

    const [txt, settxt] = useState(route?.params?.data?.addressTxt);
    console.log(route?.params?.data)

    useEffect(() => {
        _getCities()
    }, [])

    const AddAddress = () => {
        if (value) {
            addAddress(
                value,
                txt, route?.params?.data?.coordinate?.latitude,
                route?.params?.data?.coordinate?.longitude)
                .then(res => {
                    console.log(res?.Message?.ContentAr )
                    setFeedBackMessage(I18nManager.isRTL ?
                        res?.Message?.ContentAr :
                        res?.Message?.ContentEn)
                })
        } else {
            seterror(true)
        }
    }
    const _getCities = () => {
        GetCities().then(res => {
            console.log(res)
            let arr = []
            for (let i = 0; i < res?.length; i++) {
                let label = res[i]?.CityName
                let value = res[i]?.CityCode
                arr[i] = { 'label': label, 'value': value }
                console.log("arr...[]", arr)

            }
            if (arr?.length > 0) {
                setItems(arr)
            }
            setIsloading(false)
        })
    }

    function _Headers() {
        return (
            <TouchableOpacity
                onPress={() => {
                    navigation.goBack()
                }}
                style={[
                    styles?.row,
                    {
                        justifyContent: 'flex-end',
                        padding: SIZES?.Mpading
                    }]}>
                <Image
                    source={icons?.back_arrow_icon}
                    style={[
                        styles?.smallIcons,
                        {
                            alignSelf: 'flex-end',
                            tintColor: COLORS?.white
                        }
                    ]}
                />
            </TouchableOpacity>
        )
    }



    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>
            <FeedBackMsg
                msg={feedBackMessage}
                isVisible={feedBackMessage != '' ? true : false}
                Dismiss={(val) => {
                    setFeedBackMessage(val == false ? '' : feedBackMessage)
                    navigation.navigate('Address')
                }}
                hidebtn={true}
            />
            {_Headers()}

            <View
                style={{
                    ...styles?.confirmView,
                }}
            >
                <Text
                    style={{
                        ...FONTS?.h2,
                        textAlign: 'center',
                        marginVertical: SIZES?.padding15
                    }}
                >
                    {I18n.t('oneaddress')}
                </Text>
                {isloading ?
                    <Loader
                        color={COLORS?.white}
                    />
                    :

                    <DropDownPicker
                        open={open}
                        value={value}
                        items={items}
                        setOpen={setOpen}
                        setValue={setValue}
                        setItems={setItems}
                        // multiple={true}
                        placeholder={I18n.t('city')}
                        textStyle={{
                            ...FONTS?.body5
                        }}
                        onChangeValue={(value) => {
                            console.log(value);
                            seterror(false)
                        }}
                        style={{
                            // width: '85%',
                            borderRadius: SIZES?.padding,
                            backgroundColor: COLORS?.lightGray2,
                            borderColor: COLORS?.lightGray2,
                            marginVertical: SIZES?.base,
                            borderWidth: error ? 1 : 0,
                            borderColor: COLORS?.lightRed


                        }}
                        containerStyle={{
                            width: '90%',
                            alignSelf: 'center',
                            borderRadius: SIZES?.padding
                        }}
                        zIndex={60000}


                    />
                }
                <TextInput
                    multiline
                    style={{
                        width: '90%',
                        alignSelf: 'center',
                        borderRadius: 20,
                        backgroundColor: COLORS?.lightGray2,
                        height: 80

                    }}
                    value={txt}
                    onChangeText={val => {
                        settxt(val)
                    }}
                />


                <MainButton
                    title={I18n.t('saveOrders')}
                    color={COLORS?.primary}
                    titleColor={COLORS?.white}
                    BtnAction={() => {
                        AddAddress()
                        // navigation.navigate('AddAdressByMap')
                    }}
                />
            </View>
            <MainFooter
            // focused='order'
            />
        </SafeAreaView>
    )
}

export default ConfirmAddAddress;