import I18n from "i18n-js";
import React, { useEffect, useState } from "react";
import {
    SafeAreaView,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';

import {
    COLORS,
    SIZES,
    icons,
    FONTS,

} from '../../constants';
import styles from "./styles";
import MapView, { Marker } from 'react-native-maps';
import MainButton from "../UI/Button";
import MainTextInput from "../UI/TextInput";
import MainFooter from "../UI/MainFooter";
import { getCurrentPosition, handel_permissioms } from "./services/AddressServices";
import Geocoder from 'react-native-geocoding';
import { MapKey } from "../../constants/BaseURL";

/*--------------------------------------------------------*/


const AddAdressByMap = ({ navigation }) => {
    const [origin, setOrigin] = useState()
    const [coordinate, setCoordinate] = useState()
    const [addressTxt, setAddressTxt] = useState()

    useEffect(() => {
        Geocoder.init(MapKey); // use a valid API key
        getcurrentLatLong()
    }, [])

    const getcurrentLatLong = () => {
        handel_permissioms().then(res => {
            console.log("handel_permissioms", res)
            if (res) {
                getCurrentPosition().then(postion => {
                    console.log({ postion })
                    setOrigin({
                        latitude: postion.coords.latitude,
                        longitude: postion.coords.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,

                    })
                    setCoordinate({
                        latitude: postion.coords.latitude,
                        longitude: postion.coords.longitude,
                    })
                    showAddress( postion.coords.latitude, postion.coords.longitude)
                })
            }
        })
    }
    const showAddress = (lat,long) => {
        Geocoder.from(lat, long)
            .then(json => {
                var addressComponent = json.results[0];
                console.log(addressComponent?.formatted_address);
                setAddressTxt(addressComponent?.formatted_address)
            })
            .catch(error => console.warn(error));

    }
    function _Headers() {
        return (
            <TouchableOpacity
                onPress={() => {
                    navigation.goBack()
                }}
                style={[
                    styles?.row,
                    {
                        padding: SIZES?.Mpading,
                        position: 'absolute',
                        zIndex: 4000,
                        right: 10
                    }]}>
                <Image
                    source={icons?.back_arrow_icon}
                    style={[
                        styles?.smallIcons,
                        {
                            alignSelf: 'flex-end',
                            tintColor: COLORS?.white
                        }
                    ]}
                />
            </TouchableOpacity>
        )
    }
    function SearchAddress() {
        return (
            <View
                style={{
                    ...styles?.top,
                }}
            >
                <MainTextInput
                    title={I18n.t('search')}
                    righticon={icons?.search_icon}
                />
            </View >
        )
    }
    function AddAddress() {
        return (
            <View
                style={{
                    ...styles?.bottom,
                    marginVertical: SIZES?.padding,
                }}
            >
              
               <View style={{...styles?.addressView}}>
               <Text style={{...styles?.title,textAlign:'center'}}>
                     {I18n.t('oneaddress')}
                 </Text> 
                 <Text style={{...styles?.grayTxt,textAlign:'center'}}>
                     {addressTxt}
                 </Text>
               </View>
               <MainButton
                    title={I18n.t('confirmLocation')}
                    color={COLORS?.primary}
                    titleColor={COLORS?.white}
                    BtnAction={() => {
                        navigation.navigate('ConfirmAddAddress',{
                            data:{
                                coordinate,
                                addressTxt
                            }
                        })
                    }}
                    style={{
                        paddingVertical: 10
                    }}
                />
            </View>
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>
            {_Headers()}
            <View style={styles.container}>
                {SearchAddress()}
                {origin &&
                    <MapView
                        initialRegion={origin}
                        style={styles.map}
                        onPress={(event) => {
                            console.log(event.nativeEvent.coordinate)
                            setCoordinate(event.nativeEvent.coordinate)
                        }}
                    >
                        <Marker
                            coordinate={coordinate}
                            image={icons?.locationPin}
                        />
                    </MapView>}
                {AddAddress()}
            </View>
            {/* <MainFooter

                focused='order'
            /> */}
        </SafeAreaView>
    )
}

export default AddAdressByMap;

