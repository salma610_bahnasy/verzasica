import I18n from "i18n-js";
import React, { useEffect, useState } from "react";
import {
    SafeAreaView,
    View,
    Text,
    Image,
    TouchableOpacity,
    FlatList,

} from 'react-native';

import {
    COLORS,
    SIZES,
    icons,
    FONTS,

} from '../../constants';
import styles from "./styles";
import MainButton from "../UI/Button";
import MainFooter from "../UI/MainFooter";
import { Deleteddress, getAllAddress } from "./services/AddressServices";
import AsyncStorage from "@react-native-async-storage/async-storage";
import EmptyView from "../UI/EmptyView";
import OverLoader from "../UI/OverLoader";

/*--------------------------------------------------------*/


const Address = ({ navigation }) => {
    const [isLoading, setisLoading] = useState(true)
    const [address, setAddress] = useState([])
    const [overLoading, setoverLoading] = useState(false)

    useEffect(() => {
        getAddress()
    }, [])
    const getAddress = () => {
        AsyncStorage.getItem('user').then(res => {
            console.log(JSON.parse(res))
            let user = JSON.parse(res)
            getAllAddress(user?.AgentCode).then(res => {
                console.log({ res })
                setAddress(res)
            })
        })
    }
    const deleAddress = (id) => {
        setoverLoading(true)
        Deleteddress(id).then(res => {
            console.log({ res })
            getAddress()
            setoverLoading(false)

        })
    }
    function _Headers() {
        return (
            <TouchableOpacity
                onPress={() => {
                    navigation.goBack()
                }}
                style={[
                    styles?.row,
                    {
                        justifyContent: 'flex-end',
                        padding: SIZES?.Mpading
                    }]}>
                <Image
                    source={icons?.back_arrow_icon}
                    style={[
                        styles?.smallIcons,
                        {
                            alignSelf: 'flex-end',
                            tintColor: COLORS?.white
                        }
                    ]}
                />
            </TouchableOpacity>
        )
    }

    function _renderAddress() {
        const renderItem = ({ item, index }) => {
            return (
                <View
                    key={`times-:${index}`}
                    style={{
                        width: '90%',
                        alignSelf: 'center',
                        marginVertical: SIZES.smallpading,
                        padding: SIZES?.Mpading
                    }}
                    onPress={() => {

                    }}
                >
                    <View
                        style={{
                            ...styles?.row,
                            borderBottomWidth: 0.5,
                            justifyContent: 'space-between'
                        }}
                    >
                        <View
                            style={{
                                ...styles?.row,
                            }}
                        >
                            <Image
                                style={{ ...styles?.smallIcons }}
                                source={icons?.addressIcon}
                            />
                            <Text style={{
                                ...styles?.title
                            }}>
                                {I18n.t('oneaddress')}
                            </Text>
                        </View>
                        <TouchableOpacity
                            style={{
                                ...styles?.row,
                            }}
                            onPress={() => {
                                deleAddress(item?.Id)
                            }}
                        >

                            <Text style={{ ...styles?.grayTxt, color: COLORS?.lightRed }}>
                                {I18n.t('delete')}
                            </Text>
                            <Image
                                style={{ ...styles?.xSmall, tintColor: COLORS?.lightRed }}
                                source={icons?.del}
                            />
                        </TouchableOpacity>

                    </View>
                    <View style={{
                        ...styles.row,
                        marginHorizontal: SIZES?.padding
                    }}>

                        <Text style={{ ...styles?.grayTxt }}>
                            {item?.Address}
                        </Text>

                    </View>


                </View>
            )
        }
        return (
            <FlatList
                renderItem={renderItem}
                keyExtractor={item => `address-${item?.Id}`}
                data={address}
                showsVerticalScrollIndicator={false}
                ListEmptyComponent={() => {
                    return <EmptyView
                        img={icons?.addressIcon}
                        title={I18n.t('noAddress')}
                    />
                }}

            />
        )
    }
    // -----------------------------**!--!**--------------------------
    function AddAddress() {
        return (
            <View
                style={{
                    // ...styles?.bottom
                }}
            >
                <MainButton
                    title={I18n.t('addnewAddress')}
                    color={COLORS?.primary}
                    titleColor={COLORS?.white}
                    BtnAction={() => {
                        navigation.navigate('AddAdressByMap')
                    }}
                />
            </View >
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>
           <OverLoader
                isVisible={overLoading}
                Dismiss={(val) => {
                    setoverLoading(val)
                }}
            />
            {_Headers()}
            <View
                style={{
                    ...styles?.confirmView,
                }}
            >
                <Text
                    style={{
                        ...FONTS?.h2,
                        textAlign: 'center',
                        marginTop: SIZES?.padding15
                    }}
                >
                    {I18n.t('address')}
                </Text>
                {_renderAddress()}
                {AddAddress()}
            </View>
            {/* <MainFooter
                focused='order'
            /> */}
        </SafeAreaView>
    )
}

export default Address;