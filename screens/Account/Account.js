import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation } from "@react-navigation/core";
import I18n from "i18n-js";
import React, { useEffect, useState } from "react";
import {
    SafeAreaView,
    View,
    Text,
    Image,
    TouchableOpacity,
    ImageBackground,
    StyleSheet,
    I18nManager,
    Switch
} from 'react-native';
import RNRestart from 'react-native-restart'; // Import package from node modules

import {
    COLORS,
    SIZES,
    icons,
    FONTS,
    images

} from '../../constants';
import MainFooter from "../UI/MainFooter";

/*--------------------------------------------------------*/


const Account = ({ navigation }) => {
    const [isEnabled, setIsEnabled] = useState(false);
    const [profile, setProfile] = useState();

    const toggleSwitch = () => {
        setIsEnabled(previousState => !previousState)
    };
    const ChangeLang = (lang) => {
        I18n.locale == lang
        I18nManager.allowRTL(lang == 'en' ? false : true)
        I18nManager.forceRTL(lang == 'en' ? false : true)
        AsyncStorage.setItem('language', lang, () => {
            RNRestart.Restart();
        })
    };

    useEffect(() => {
        _getUser()
    }, []);

    const _getUser = () => {
       AsyncStorage.getItem('user').then(res=>{
           console.log(JSON.parse(res))
           setProfile(JSON.parse(res))

       })
    };

    function BtnView(title, icon, navigate) {
        return (
            <TouchableOpacity
                style={
                    Styles?.btn
                }
                onPress={() => {
                    if(navigate=="Registration"){
                        AsyncStorage.removeItem("user").then(
                            navigation?.navigate(navigate, {

                            })
                        )
                      
                    }else{
                    navigation?.navigate(navigate, {

                    })
                }
                }}
            >
                <View style={Styles?.row}>
                    <Image
                        source={icon}
                        style={
                            Styles?.smallIcon
                        }
                    />
                    <Text style={{
                        ...Styles?.whiteTxt
                    }}>
                        {title}
                    </Text>
                </View>
                <Image
                    source={
                        I18nManager?.isRTL ?
                            icons?.backen :
                            icons?.backar}
                    style={Styles?.primeIcon}
                />
            </TouchableOpacity>
        )
    }
    
    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>

            <ImageBackground
                source={images?.splash}
                style={{ ...Styles?.slider }}>
                <TouchableOpacity
                    style={
                        Styles?.avatr
                    }
                >
                    <Image
                        source={icons?.user}
                        style={{
                            width: '80%',
                            height: '60%',
                            tintColor:COLORS?.primary,
                            resizeMode:'contain'
                         
                        }}
                    />
                </TouchableOpacity>
                <Text style={Styles?.primaryTxt}>
                   {profile?.AgentName}
                </Text>
            </ImageBackground>
            {/* btn container */}

            {BtnView(I18n.t('Favorite'), icons?.fav, 'Favorite')}
            {BtnView(I18n.t('address'), icons?.settings, 'Address')}

            {BtnView(I18n.t('logout'), icons?.logout, 'Registration')}
            <View style={Styles.container2}>
                <Text
                    style={{
                        ...FONTS?.body3
                    }}
                >
                    {I18n.locale == 'ar' ? I18n.t('arabic') : I18n.t('english')}
                </Text>
                <Switch
                    onValueChange={(val) => {
                        console.log("val", val)
                        toggleSwitch()
                        ChangeLang(I18n.locale == 'ar' ? 'en' : 'ar')
                        // Linking.openSettings();
                    }}
                    value={isEnabled}
                />
            </View>

            <MainFooter
                focused='account'
            />
        </SafeAreaView>
    )
}

export default Account;








const Styles = StyleSheet.create({
    container: { width: '100%', height: SIZES?.height },

    whiteTxt: {
        ...FONTS?.h4,
        color: COLORS?.gray50,
        marginHorizontal: 10,
        lineHeight: 35,
        textTransform: 'capitalize'


    },
    primaryTxt: {
        ...FONTS?.h3,
        color: COLORS?.white,
        textAlign: 'center',
        marginTop: SIZES?.base,
    },

    slider: {
        width: '100%',
        height: 200,
        alignItems: 'center',
        justifyContent: 'flex-end',
        alignContent: 'center',
        marginBottom: 50
    },
    btn: {
        backgroundColor: COLORS?.white,
        width: '95%',
        alignSelf: 'center',
        paddingVertical: SIZES?.base,
        paddingHorizontal: 5,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 10,
        justifyContent: 'space-between',
        marginVertical: SIZES?.base
    },
    avatr: {
        width: 100,
        height: 100,
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: COLORS?.black,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        backgroundColor: '#fff'

    },
    primeIcon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        tintColor: COLORS?.primary,
    },
    smallIcon: {
        width: 20,
        height: 20,
        resizeMode: 'contain',
        tintColor: COLORS?.gray50,
        marginHorizontal: SIZES?.base
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    container2: {
        alignItems: "center",
        justifyContent: "center",
        marginTop: SIZES?.radius,
        flexDirection: 'row',
        alignItems: 'center'

    }


})