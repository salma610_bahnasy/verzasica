import I18n from "i18n-js";
import React, { useState, useEffect } from "react";
import {
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    Image,
    FlatList,
    I18nManager,
    TextInput
} from 'react-native';

import {
    COLORS,
    SIZES,
    icons,
    FONTS,
    images
} from '../../constants';
import styles from "./styles";
import MainButton from "../UI/Button";
import MainFooter from "../UI/MainFooter";
import { getCartItmsNum } from "../../constants/Global";
import OverLoader from "../UI/OverLoader";
import AsyncStorage from "@react-native-async-storage/async-storage";


/*--------------------------------------------------------*/


const Cart = ({ navigation }) => {
    const [isloading, setIloading] = useState(false)
    const [itemsInCart, setItemsInCart] = useState([])
    const [overLoader, setOverLoader] = useState(false)
    const [totalValue, setTotalValue] = useState('')
    const [orderSaved, setOrderSaved] = useState('')


    useEffect(() => {
        _getCartItms()
    }, [])

    const _getCartItms = async () => {
        let cartItems = await getCartItmsNum();
        AsyncStorage.getItem('orderSaved').then(res => {
            console.log('orderSaved', res)
            setOrderSaved(res)
        })
        console.log({ cartItems })
        setIloading(false)
        if (cartItems != null) {
            setItemsInCart(cartItems)
            _calculatAllCartPrice(cartItems)
        }
    }
    const _delartItms = async (id) => {
        setOverLoader(true)
        let mdifiedArr = itemsInCart;
        for (let i = 0; i < mdifiedArr?.length; i++) {
            if (id == mdifiedArr[i]?.ServiceCode) {
                mdifiedArr?.splice(i, 1)
                console.log("after remove", mdifiedArr)
            }
        }
        await AsyncStorage.setItem("itemsInCart", JSON.stringify(itemsInCart), () => {
            _getCartItms()
            setOverLoader(false)
        })



    }
    const _editItem = async (item) => {
        let mdifiedArr = itemsInCart;
        for (let i = 0; i < mdifiedArr?.length; i++) {
            if (item.ServiceCode == mdifiedArr[i]?.ServiceCode) {
                mdifiedArr[i] = item
            }
        }

        await AsyncStorage.setItem("itemsInCart", JSON.stringify(itemsInCart), () => {
            _getCartItms()
        })



    }
    const _calculatAllCartPrice = (cartItems) => {
        let x = [];
        for (let i = 0; i < cartItems?.length; i++) {
            x[i] = cartItems[i].Price * cartItems[i].qnt
        }
        if (x) {
            const sum = x.reduce((partialSum, a) => partialSum + a, 0);
            console.log(sum); // 6
            console.log({ x })
            setTotalValue(sum)
        }
    }


    // ------------------------------------------
    function _Headers() {
        return (
            <View
                style={[
                    styles?.row,
                    {
                        justifyContent: 'flex-end',
                        paddingHorizontal: SIZES?.Mpading
                    }]}>
                <Image
                    source={images?.logo}
                    style={[
                        styles?.smallIcons,
                        {
                            alignSelf: 'flex-end'
                        }
                    ]}
                />
            </View>
        )
    }

    function _ordersView() {
        const renderItem = ({ item, index }) => {
            return (
                <View
                    key={`times-:${index}`}
                    style={{ ...styles?.cartItemStyle }}
                    onPress={() => {

                    }}
                >
                    <View
                        style={{ ...styles?.row }}

                    >
                        <Text
                            style={{
                                ...FONTS?.h3,
                                color: COLORS?.primary
                            }}
                        >
                            {item?.ServiceName}
                        </Text>

                    </View>
                    <View
                        style={{ ...styles?.row, justifyContent: 'flex-end' }}

                    >
                        <Text
                            style={{
                                ...FONTS?.body4
                            }}
                        >
                            {I18n.t('qnt')}
                        </Text>
                        <View
                            style={{
                                ...styles?.qntView,
                                ...styles?.row,
                            }}

                        >
                            <TouchableOpacity
                                style={{
                                    ...styles?.xIconView
                                }}
                                onPress={() => {
                                    item.qnt = item?.qnt + 1
                                    console.log({ item })
                                    _editItem(item)
                                }}
                            >
                                <Image
                                    style={{
                                        ...styles?.xIcon
                                    }}
                                    source={icons?.plus_icon}
                                />
                            </TouchableOpacity>
                            <Text
                                style={{
                                    ...FONTS?.body5,
                                    marginHorizontal: 10
                                }}
                            >{item?.qnt}</Text>
                            <TouchableOpacity
                                style={{
                                    ...styles?.xIconView
                                }}
                                onPress={() => {
                                    if (item.qnt <= 0) {
                                        item.qnt = item?.qnt - 1
                                        _editItem(item)
                                    } else {
                                        item.qnt = item?.qnt - 1
                                        _editItem(item)
                                    }


                                }}
                            >
                                <Image
                                    style={{
                                        ...styles?.xIcon

                                    }}
                                    source={icons?.minus}
                                />
                            </TouchableOpacity>
                        </View>



                        <Text style={{
                            ...FONTS?.body4
                        }}>{item?.qnt * item?.Price}</Text>
                        <Text style={{
                            ...FONTS?.body4
                        }}>ريال</Text>



                    </View>
                    <View
                        style={{
                            ...styles?.row,
                            justifyContent: 'space-between',
                            marginHorizontal: 20,
                            marginTop: SIZES?.smallpading
                        }}

                    >
                        <TouchableOpacity>
                            <Image
                                style={{
                                    width: 20,
                                    height: 20,
                                    resizeMode: 'contain'
                                }}
                                source={icons?.fav}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                _delartItms(item?.ServiceCode)
                            }}
                        >
                            <Text
                                style={{
                                    ...FONTS?.body3,
                                    color: COLORS?.secondary
                                }}
                            >
                                {I18n.t('delete')}

                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
        return (
            <FlatList
                renderItem={renderItem}
                keyExtractor={item => `ServiceCode-${item?.ServiceCode}`}
                data={itemsInCart}
                ListEmptyComponent={_emptyView()}
            />
        )
    }
    function _emptyView() {
        return <View style={{ ...styles?.center }}>
            <Text style={{ ...FONTS?.h4 }}>
                {I18n.t('noItems')}
            </Text>
        </View>
    }
    function _checkOutView() {
        if (itemsInCart?.length > 0) {
            return <View
                style={{
                    ...styles?.checkoutView
                }}

            >
                <View
                    style={{
                        ...styles?.row,
                        justifyContent: 'space-between'
                    }}>
                    <Text
                        style={{
                            ...FONTS?.body3,
                            color: COLORS?.secondary
                        }}
                    >
                        {I18n.t('total')}
                    </Text>
                    <Text
                        style={{
                            ...FONTS?.body5,
                            color: COLORS?.gray
                        }}
                    >
                        {I18n.t('withvat')}
                    </Text>
                    <View style={{ ...styles?.row }}>
                        <Text
                            style={{
                                ...FONTS?.body3,
                                color: COLORS?.secondary

                            }}
                        >
                            {totalValue}
                        </Text>
                        <Text
                            style={{
                                ...FONTS?.body3
                            }}
                        >
                            {I18n.t('rs')}
                        </Text>
                    </View>
                </View>

                <MainButton
                    title={I18n.t('continuepayment')}
                    color={isloading ? COLORS?.lightGray3 : COLORS?.primary}
                    titleColor={COLORS?.white}
                    BtnAction={() => {
                        if (orderSaved=='true') {
                            navigation.navigate('ConfirmCheckOut')
                        } else {
                            navigation.navigate('ConfirmOrder')
                        }
                    }}
                    style={{
                        marginTop: 10,
                        paddingVertical: 15
                    }}
                />

            </View>
        }
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>
            <OverLoader
                isVisible={overLoader}
                Dismiss={(val) => {
                    setOverLoader(val)
                }}
            />
            {_Headers()}
            <View
                style={styles?.confirmView}
            >
                <Text
                    style={{
                        ...styles?.borderTitle
                    }}
                >
                    {I18n.t('cart')}
                </Text>
                {_ordersView()}
                <View style={{ height: 100 }} />
                {_checkOutView()}
            </View>
            <MainFooter

                focused='order'
            />
        </SafeAreaView>
    )
}

export default Cart;