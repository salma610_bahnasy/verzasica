import I18n from "i18n-js";
import React, { useState } from "react";
import {
    SafeAreaView,
    View,
    Text,
    Image,
    TouchableOpacity,
} from 'react-native';

import {
    COLORS,
    SIZES,
    FONTS,
    images,
    icons
} from '../../constants';
import styles from "./styles";
import MainButton from "../UI/Button";
import MainFooter from "../UI/MainFooter";
import { CheckBox } from "react-native-elements";


/*--------------------------------------------------------*/


const ConfirmCheckOut = ({ navigation }) => {
    const [isloading, setIloading] = useState(false)


    function _Headers() {
        return (
            <View
                style={[
                    styles?.row,
                    { justifyContent: 'flex-end', paddingHorizontal: SIZES?.Mpading }]}>
                <Image
                    source={images?.logo}
                    style={[
                        styles?.smallIcons,
                        {
                            tintColor: COLORS?.white,
                            alignSelf: 'flex-end'
                        }
                    ]}
                />
            </View>
        )
    }

    function _paymentTypeView() {

        return (
            <View
                style={{
                    marginHorizontal: SIZES?.padding,
                    marginVertical: SIZES?.smallpading,

                }}
            >
                <View
                    style={{ ...styles.row }}
                >
                    <Image
                        style={{ ...styles.xSmall }}
                        source={icons.wallet}
                    />
                    <Text
                        style={{
                            ...FONTS?.h4,
                            marginHorizontal: SIZES?.base,

                        }}
                    >
                        {I18n.t('paymentWay')}
                    </Text>
                </View>

                <TouchableOpacity style={{ ...styles.row }}>
                    <CheckBox />
                    <Text style={{ ...FONTS?.h5 }}>
                        الدفع عند الإستلام
                    </Text>

                </TouchableOpacity>
                <TouchableOpacity style={{ ...styles.row }}>
                    <CheckBox />
                    <Text style={{ ...FONTS?.h5 }}>
                        فيزا
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ ...styles.row }}>
                    <CheckBox />
                    <Text style={{ ...FONTS?.h5 }}>
                        مدي
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
    function _AddressView() {

        return (
            <View style={{
                marginHorizontal: SIZES?.padding,
                marginVertical: SIZES?.smallpading,
            }}>
                <View
                    style={{ ...styles.row }}
                >
                    <Image
                        style={{ ...styles.xSmall }}
                        source={icons.locationPin}
                    />
                    <Text
                        style={{
                            ...FONTS?.h4,
                            marginHorizontal: SIZES?.base,

                        }}
                    >
                        {I18n.t('oneaddress')}
                    </Text>
                </View>
                <Text
                    style={{
                        ...FONTS?.body5,
                        marginHorizontal: SIZES?.base,
                        alignSelf: 'flex-start',
                        textAlign: 'left'
                    }}

                >
                    somoha ,alex,10 fawzy moaaz
                </Text>
                <Text
                    style={{
                        ...FONTS?.h5,
                        margin: SIZES?.base,
                        color: COLORS?.primary

                    }}
                >
                    {I18n.t('changeaddress')}
                </Text>
                <View
                    style={{
                        width: '80%',
                        borderBottomWidth: .5,
                        alignSelf: 'center',
                        marginTop: SIZES?.smallpading,
                        borderColor: COLORS?.primary
                    }}
                />
            </View>
        )
    }
    function _checkOutView() {
        return <View
            style={{
                ...styles?.checkoutView
            }}

        >
            <View
                style={{
                    ...styles?.row,
                    ...styles?.checkoutViewRow
                }}>
                <Text
                    style={{
                        ...FONTS?.body3,
                        color: COLORS?.secondary
                    }}
                >
                    {I18n.t('total')}
                </Text>
                <Text
                    style={{
                        ...FONTS?.body5,
                        color: COLORS?.gray
                    }}
                >
                    {I18n.t('withvat')}
                </Text>
                <View style={{ ...styles?.row }}>
                    <Text
                        style={{
                            ...FONTS?.body3,
                            color: COLORS?.secondary

                        }}
                    >
                        1000
                    </Text>
                    <Text
                        style={{
                            ...FONTS?.body3
                        }}
                    >
                        {I18n.t('rs')}
                    </Text>
                </View>
            </View>

            <MainButton
                title={I18n.t('confirmOrder')}
                color={isloading ? COLORS?.lightGray3 : COLORS?.primary}
                titleColor={COLORS?.white}
                BtnAction={() => {
                    navigation.navigate("Home")
                }}
                style={{
                    marginTop: 10,
                    paddingVertical: 10
                }}
            />

        </View>
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>

            {_Headers()}
            <View
                style={styles?.confirmView}
            >
                <Text
                    style={{
                        ...styles?.borderTitle
                    }}
                >
                    {I18n.t('Continuetopay')}
                </Text>
                {_AddressView()}
                {_paymentTypeView()}
                <View style={{ height: 100 }} />
                {_checkOutView()}
            </View>
            <MainFooter

                focused='order'
            />
        </SafeAreaView>
    )
}

export default ConfirmCheckOut;