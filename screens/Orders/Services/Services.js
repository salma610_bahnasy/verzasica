import { BaseUrL } from "../../../constants/BaseURL"

const getCats = () => {
    return new Promise((resolve, reject) => {
        console.log(`${BaseUrL}ServiceCategoryApi/GetData`)
        fetch(`${BaseUrL}ServiceCategoryApi/GetData`, {
            method: 'GET',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("getCats", responseJson)
                resolve(responseJson)
            })


    })
}
const getServiceBycatId = (id) => {
    return new Promise((resolve, reject) => {
        fetch(`${BaseUrL}ServiceApi/GetServiceByCategory/${id}`, {
            method: 'GET',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("getLiveTripPPPP", responseJson)
                resolve(responseJson?.ReturnData)
            })


    })
}
const Search= (id,search) => {
    return new Promise((resolve, reject) => {
        fetch(`${BaseUrL}ServiceApi/GetServiceByCategory/${id}/${search}`, {
            method: 'GET',
            headers: {
                "Content-Type": 'application/json',
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("getLiveTripPPPP", responseJson)
                resolve(responseJson?.ReturnData)
            })


    })
}

const getAvailableTime= (OrderDate,ServiceCodes,CityCode) => {
    return new Promise((resolve, reject) => {
        let data={
            CityCode,
            OrderDate,
            ServiceCodes
        }
        console.log({data})
        fetch(`${BaseUrL}OrderApi/AvailableTime`, {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json',
            },
            body:JSON.stringify(data)
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("getAvailableTime", responseJson)
                resolve(responseJson)
            })


    })
}
const saveOrder= (timeCode,user,date,services) => {
    return new Promise((resolve, reject) => {
        let data={
            "OrderCode": 0,
            "TbAgent": user,
            "VisitDate": date,
            "CityCode": 12,
            "Notes": "",
            "TbOrderService": services,
            "TbOrderLocation": [
                {
                    "lat": 24.2222,
                    "lng": 32.2222
                }
            ]
        }
        console.log({data})
        fetch(`${BaseUrL}OrderApi/saveOrder/${timeCode}`, {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json',
            },
            body:JSON.stringify(data)
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log("saveOrder", responseJson)
                resolve(responseJson)
            })


    })
}
export {
    getCats,
    getServiceBycatId,
    Search,
    getAvailableTime,
    saveOrder
}