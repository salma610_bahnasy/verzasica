import I18n from "i18n-js";
import React, { useEffect, useState } from "react";
import {
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    Image,
    FlatList,
    I18nManager,
    TextInput
} from 'react-native';

import {
    COLORS,
    SIZES,
    icons,
    FONTS
} from '../../constants';
import styles from "./styles";
import AddOrderModal from "../UI/AddOrderModal";
import LinearGradient from 'react-native-linear-gradient';
import MainFooter from "../UI/MainFooter";
import { getCats, getServiceBycatId, Search } from "./Services/Services";
import Loader from "../UI/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { getCartItmsNum } from "../../constants/Global";
import OverLoader from "../UI/OverLoader";
import SelectCity from "../UI/SelectCity";

/*--------------------------------------------------------*/


const Orders = ({ navigation }) => {
    const [selectedTab, setSelectedTab] = useState('1')
    const [showAddOrderModal, setShowAddOrderModal] = useState(false)
    const [tabs, setTabs] = useState([])
    const [service, setServices] = useState([])
    const [loading, setLoading] = useState(true)
    const [qnt, setQnt] = useState(1)
    const [selectedService, setSelectedService] = useState(1)
    const [searchTxt, setSearchTxt] = useState('')
    const [selectedOrderItem, setSelectedOrderItem] = useState('')
    const [itemsInCart, setItemsInCart] = useState([])
    const [overLoader, setOverLoader] = useState(false)
    const [selectedCity, setSelectedCity] = useState(false)

    useEffect(() => {
        AsyncStorage.getItem('cityCode').then(city => {
            console.log({ city })
            if (city == null) {
                setSelectedCity(true)
            }
        })
    }, [])

    useEffect(() => {
        if (overLoader) {
            AsyncStorage.setItem("itemsInCart", JSON.stringify(itemsInCart), () => {
                console.log(JSON.stringify(itemsInCart))
                setOverLoader(false)
                setShowAddOrderModal(true)

            })// This is be executed when `loading` state changes
        }
    }, [overLoader])

    useEffect(() => {
        _setCartItms()
        _getCat()
    }, [])
    const _setCartItms = () => {
        getCartItmsNum().then(cartItems => {
            console.log("////", cartItems.length)
            if (cartItems != null) {
                setItemsInCart(cartItems)
            }
        })
    }

    const _getCat = () => {
        getCats().then(res => {
            console.log("order cats", res)
            setTabs(res)
            setSelectedTab(res[0]?.ServiceCategoryCode)
            _getServiceByCat(res[0]?.ServiceCategoryCode)

        })
    }
    const _getServiceByCat = (id) => {
        getServiceBycatId(id).then(res => {
            console.log("order service", res)
            setServices(res)
            setLoading(false)

        })
    }
    const _Search = (search) => {
        Search(selectedTab, search).then(res => {
            console.log("_Search", res)
            setServices(res)
            // setLoading(false)

        })
    }
    const _addToCart = async (item) => {

        setOverLoader(true)
        for (let i = 0; i < itemsInCart?.length; i++) {
            if (itemsInCart[i]?.ServiceCode == item?.ServiceCode) {
                // item.qnt = item.qnt + 1
                itemsInCart[i].qnt = itemsInCart[i].qnt + 1
                item.qnt = itemsInCart[i].qnt
                console.log(itemsInCart[i].qnt, item.qnt)
                setSelectedOrderItem(item)
                setItemsInCart(itemsInCart);
                return itemsInCart
            }
        }
        item.qnt = selectedService == item?.ServiceCode ? qnt : 1
        console.log({ item })
        setSelectedOrderItem(item)
        setItemsInCart(prev => [...prev, item]);
        console.log({ itemsInCart })

    }

    function _Headers() {
        return (
            <View
                style={[
                    styles?.row,
                    styles?.locationView]}>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('Address')
                    }}
                    style={styles?.row}>
                    <Image
                        source={icons?.locationPin}
                        style={[
                            styles?.smallIcons,
                            { tintColor: COLORS?.white }
                        ]}
                    />
                    <Text style={{
                        ...styles?.grayTxt
                    }}>
                        {I18n.t('location')}
                    </Text>
                    <Text
                        style={{
                            ...styles?.whiteTxt
                        }}>
                        الرياض
                    </Text>
                    <Image
                        source={icons?.downtraing}
                        style={[
                            styles?.xSmall
                        ]}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        AsyncStorage.getItem('user').then(res => {
                            if (res != null) {
                                navigation.navigate("Cart")
                            } else {
                                navigation.navigate("Registration")
                            }
                        })
                    }}
                >
                    <Text
                        style={{
                            ...styles?.cartbadg
                        }}
                    >{itemsInCart?.length}</Text>
                    <Image
                        source={icons?.orders}
                        style={[
                            styles?.smallIcons,
                            { tintColor: COLORS?.white }
                        ]}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    function _renderSideTabs() {
        const renderItem = ({ item, index }) => {
            return (
                <TouchableOpacity
                    onPress={() => {
                        setSelectedTab(item?.ServiceCategoryCode)
                        _getServiceByCat(item?.ServiceCategoryCode)
                    }}
                    key={`tabs-${index}`}
                    style={{
                        ...styles?.tabView,
                        borderWidth:
                            selectedTab == item?.ServiceCategoryCode
                                ? 1 : 0,
                    }}

                >
                    <Text
                        numberOfLines={1}
                        style={{
                            ...styles?.whiteTxt
                        }}>
                        {item?.ServiceCategoryName}
                    </Text>
                    {selectedTab == item?.ServiceCategoryCode &&
                        <Image
                            source={I18nManager.isRTL ?
                                icons?.artraing :
                                icons?.entraing}
                            style={{
                                ...styles?.tabArrow
                            }}
                        />}
                </TouchableOpacity>
            )
        }

        return (
            <FlatList
                data={tabs}
                renderItem={renderItem}
                keyExtractor={item => `${item?.ServiceCategoryCode}`}
                showsVerticalScrollIndicator={false}

            />
        )
    }

    function _renderServicesView() {
        const renderItem = ({ item, index }) => {
            return (
                <View
                    onPress={() => {

                    }}
                    key={`services-${index}`}
                    style={{ ...styles?.servicesView, justifyContent: 'space-between' }}

                >
                    <View style={{ flex: 2, height: '100%' }}>
                        <Text
                            numberOfLines={2}
                            style={{
                                ...FONTS?.h5,
                                flexShrink: 1,
                                fontSize: 9,
                                position: 'absolute',
                                top: 5,
                                lineHeight: 16
                            }}>
                            {item?.ServiceName}
                        </Text>
                        {/* <View style={{
                            ...styles?.row,
                            justifyContent: 'flex-end'
                        }}>
                            <Text style={{ ...FONTS?.body5 }}>
                                {item?.Price}
                            </Text>
                            <Text style={{ ...FONTS?.body6 }}>
                                ريال
                            </Text>
                        </View> */}
                        <TouchableOpacity
                            onPress={async () => {
                                _addToCart(item)
                            }}
                            style={{
                                ...styles?.addServicesBtn
                            }}
                        >
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={['#536c68', '#21ba9f']}
                                style={{
                                    ...styles?.LinearGradient
                                }}
                                

                            >

                                <Image
                                    source={icons?.plus_icon}
                                    style={[
                                        styles?.xSmall,
                                        { tintColor: COLORS?.white }
                                    ]}
                                />
                            </LinearGradient>
                        </TouchableOpacity>

                    </View>
                    <View
                        style={[
                            styles?.row,
                            styles?.countView, { flex: 1 }
                        ]}>
                        <TouchableOpacity
                            onPress={() => {
                                setSelectedService(item?.ServiceCode)
                                if (qnt <= 0) {
                                    setQnt(0)
                                } else {
                                    setQnt(qnt - 1)
                                }
                            }}
                            style={{
                                ...styles?.qntbtn
                            }}
                        >
                            <Image
                                source={icons?.minus}
                                style={{
                                    ...styles?.xSmall,
                                    tintColor: COLORS?.white
                                }}
                            />
                        </TouchableOpacity>
                        <Text
                            style={[
                                styles?.qtntxt
                            ]}
                        >
                            {selectedService == item?.ServiceCode ? qnt : 1}
                        </Text>
                        <TouchableOpacity
                            onPress={() => {
                                setSelectedService(item?.ServiceCode)
                                setQnt(qnt + 1)
                            }}
                            style={{
                                ...styles?.qntbtn
                            }}
                        >
                            <Image
                                source={icons?.plus_icon}
                                style={{
                                    ...styles?.xSmall,
                                    tintColor: COLORS?.white
                                }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={{ ...FONTS?.h5, fontSize: 9 }}>
                            القيمة
                        </Text>
                        <View style={{ ...styles?.row }}>
                            <Text
                                style={{ ...styles?.price, fontSize: 9 }}>
                                {item?.Price}
                            </Text>
                            <Text
                                style={{ ...FONTS?.body6, fontSize: 9 }}
                            >
                                الريال
                            </Text>
                        </View>
                    </View>
                </View>
            )
        }

        return (
            <FlatList
                data={service}
                renderItem={renderItem}
                keyExtractor={item => `${item?.ServiceCode}`}
                showsVerticalScrollIndicator={false}

            />
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>
            <SelectCity
                isVisible={selectedCity}
                Dismiss={(val) => {
                    setSelectedCity(val)
                }}
            />
            <AddOrderModal
                isVisible={showAddOrderModal}
                Dismiss={(val) => {
                    setShowAddOrderModal(val)
                }}
                selectedOrderItem={selectedOrderItem}
            />
            <OverLoader
                isVisible={overLoader}
                Dismiss={(val) => {
                    setOverLoader(val)
                }}
            />
            {_Headers()}
            {loading ?
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Loader
                        color={COLORS?.white}
                        size={"large"}
                    />
                </View>
                : <View
                    style={{ ...styles?.mainView }}
                >
                    <View
                        style={{
                            flex: 1,
                            height: "90%",
                            // marginBottom:50,
                            alignSelf: 'center'

                        }}
                    >
                        {_renderSideTabs()}

                    </View>
                    <View style={{
                        flex: 3,
                        height: "85%",
                        backgroundColor: COLORS?.white,
                        alignSelf: 'flex-start',
                        borderRadius: SIZES?.radius

                    }}
                    >
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            backgroundColor: COLORS?.offWhite,
                            width: '90%',
                            borderRadius: 40,
                            alignSelf: 'center',
                            marginTop: SIZES?.Mpading,
                            paddingHorizontal: SIZES?.Mpading,
                            height: 40
                        }}>
                            <TextInput
                                placeholder={I18n.t('sreachForServices')}
                                style={{
                                    flex: 1,
                                    ...FONTS?.body5,
                                    lineHeight: 22,
                                    alignSelf: 'center'
                                }}
                                onChangeText={(val) => {
                                    if (val != "") {
                                        setSearchTxt(val)
                                        _Search(val)
                                    }
                                }}
                            />
                            <Image
                                source={icons?.search_icon}
                                style={{
                                    width: 20,
                                    height: 20,

                                }}
                            />
                        </View>

                        {_renderServicesView()}

                    </View>
                </View>
            }
            <MainFooter
            // focused='order'
            />
        </SafeAreaView>
    )
}

export default Orders;