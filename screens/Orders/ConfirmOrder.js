import I18n from "i18n-js";
import React, { useEffect, useState } from "react";
import {
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    Image,
    FlatList,
    I18nManager,
    TextInput
} from 'react-native';

import {
    COLORS,
    SIZES,
    icons,
    FONTS
} from '../../constants';
import styles from "./styles";
import AddOrderModal from "../UI/AddOrderModal";
import LinearGradient from 'react-native-linear-gradient';
import CalendarPicker from 'react-native-calendar-picker';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import AppointmentsModal from "../UI/AppointmentsModal";
import MainFooter from "../UI/MainFooter";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { getAvailableTime, saveOrder } from "./Services/Services";
import { getCartItmsNum } from "../../constants/Global";
import OverLoader from "../UI/OverLoader";
import FeedBackMsg from "../UI/FeedBackMsg";


/*--------------------------------------------------------*/


const ConfirmOrder = ({ navigation }) => {
    const [selectedTab, setSelectedTab] = useState('1')
    const [showAddOrderModal, setShowAddOrderModal] = useState(false)
    const [tab, setTab] = useState('availableTime')
    const [showAppointmentsModal, setShowAppointmentsModal] = useState(false)
    const [selectedDate, setSelectedDate] = useState('')
    const [minday, setminday] = useState('')
    const [time, setTime] = useState([])
    const [ServiceCodes, setServiceCodes] = useState([])
    const [CityCode, setCityCode] = useState(12)
    const [overLoader, setOverLoader] = useState(false)
    const [feedBackMessage, setFeedBackMessage] = useState('')
    const [services, setServices] = useState([])
    const [user, setUser] = useState(null)

    useEffect(() => {
        getServices()
        const today = new Date()
        const tomorrow = new Date(today)
        tomorrow.setDate(tomorrow.getDate() + 1)
        console.log("tomorrow", tomorrow)
        setminday(tomorrow)

    }, [])

    const getServices = () => {
        AsyncStorage.getItem('user').then(res => {
            setUser(JSON.parse(res))
            
        })
        getCartItmsNum().then(service => {
            console.log({ service })
            setServices(service)
            let IDS = service.map(s => s.ServiceCode)
            console.log({ IDS })
            setServiceCodes(IDS)
        })
    }
    const onDateChange = (date) => {
        console.log("data....", date)
    }

    function _Headers() {
        return (
            <TouchableOpacity
                onPress={() => {
                        if (user != null) {
                            navigation.navigate("Cart")
                        } else {
                            navigation.navigate("Registration")
                        }
                }}
                style={{
                    alignSelf: 'flex-end',
                    marginHorizontal: SIZES?.padding,
                    marginVertical: SIZES?.smallpading
                }}
            >
                <Text
                    style={{
                        ...styles?.cartbadg
                    }}
                >{ServiceCodes?.length}</Text>
                <Image
                    source={icons?.orders}
                    style={[
                        styles?.smallIcons,
                        { tintColor: COLORS?.white }
                    ]}
                />
            </TouchableOpacity>
        )
    }

    function _TabsView() {
        return (<View
            style={
                styles?.RowTabView
            }
        >
            <TouchableOpacity
                onPress={() => {
                    setTab('orders')
                }}
                style={{
                    ...styles?.tabBtn,
                    borderBottomWidth: tab == "orders" ? 1 : 0,
                    borderColor: COLORS?.primary
                }}
            >
                <Text style={{
                    ...FONTS?.h4,
                    color: tab == "orders" ?
                        COLORS?.black :
                        COLORS?.lightGray3

                }}>
                    {I18n.t('orderedService')}
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => {
                    setTab('availableTime')
                }}
                style={{
                    ...styles?.tabBtn,
                    borderBottomWidth: tab == "availableTime" ? 1 : 0,
                    borderColor: COLORS?.primary
                }}
            >
                <Text style={{
                    ...FONTS?.h4,
                    color: tab == "availableTime" ?
                        COLORS?.black :
                        COLORS?.lightGray3
                }}>
                    {I18n.t('availableTime')}

                </Text>
            </TouchableOpacity>

        </View>)

    }

    function _calenderView() {
        return <View style={{ marginTop: SIZES?.padding20 }}>
            <Calendar
                onDayPress={(day) => {
                    console.log('selected day', day)
                    setShowAppointmentsModal(true)
                    alert('true')
                }}
                onDayChange={(day) => {
                    console.log('selected day', day)
                    setShowAppointmentsModal(true)
                    alert('onDayChange')
                }}
                onDayLongPress={(day) => { console.log('selected day', day) }}
                monthFormat={'yyyy MMM'}
                onMonthChange={(month) => { console.log('month changed', month) }}
                hideArrows={false}
                // renderArrow={(direction) => (<Arrow />)}
                hideExtraDays={false}
                disableMonthChange={false}
                firstDay={1}
                hideDayNames={false}
                showWeekNumbers={false}
                onPressArrowLeft={subtractMonth => subtractMonth()}
                onPressArrowRight={addMonth => addMonth()}
                enableSwipeMonths={true}
                dayComponent={({ date, state, marking }) => {
                    return (
                        <TouchableOpacity
                            style={{

                                backgroundColor: state == "disabled" ?
                                    COLORS?.lightGray3 :
                                    COLORS.white,
                                ...styles?.day
                            }}
                            onPress={() => {
                                console.log({ state })
                                if (state != "disabled") {
                                    console.log("dayPressed")
                                    setOverLoader(true)
                                    getAvailableTime(date?.dateString, ServiceCodes, CityCode).then(res => {
                                        console.log({ res })
                                        if (res?.ReturnData) {
                                            setShowAppointmentsModal(!showAppointmentsModal)
                                            setTime(res?.ReturnData)
                                        } else {
                                            setFeedBackMessage(I18nManager.isRTL ? res?.Message?.ContentAr : res?.Message?.ContentEn)
                                        }

                                        setOverLoader(false)
                                    })
                                    setSelectedDate(date?.dateString)
                                    AsyncStorage.setItem('selectedDate', JSON.stringify(date?.dateString), () => {
                                        console.log(date)
                                    })
                                }
                            }}
                        >
                            <Text style={{
                                textAlign: 'center',
                                color:
                                    state == "disabled" ?
                                        COLORS?.white :
                                        COLORS?.primary
                            }}>
                                {date.day}
                            </Text>
                        </TouchableOpacity>
                    );
                }}
                // markingType='custom'
                // markedDates={{
                //     '2021-12-15': { state: 'disabled' },
                //     '2021-12-16': { state: 'disabled' },
                // }}
                onDateChange={() => {
                    alert('te')
                }}
                minDate={minday}

            />
            {/* indicators */}
            <View style={{
                ...styles?.row,
                marginTop: SIZES?.Mpading
            }}>

                <View style={{ ...styles?.row }}>
                    <View
                        style={{
                            backgroundColor:
                                COLORS?.white,
                            ...styles.indicator

                        }}
                    />
                    <Text
                        style={{
                            ...FONTS?.body3
                        }}
                    >{I18n.t('available')}</Text>
                </View>
                <View style={{
                    ...styles?.row,
                    marginLeft: SIZES?.padding15
                }}>
                    <View
                        style={{
                            backgroundColor:
                                COLORS?.lightGray3,
                            ...styles.indicator

                        }}
                    />
                    <Text
                        style={{
                            ...FONTS?.body3
                        }}
                    >{I18n.t('notavailable')}</Text>
                </View>
            </View>
        </View>
    }
 
    function _orderView() {
        const renderItem = ({ item, index }) => {
            return (
                <View
                    onPress={() => {

                    }}
                    key={`services-${index}`}
                    style={{ ...styles?.servicesView, justifyContent: 'space-between' }}

                >
                    <View style={{ flex: 2, height: '100%' }}>
                        <Text
                            numberOfLines={2}
                            style={{
                                ...FONTS?.h5,
                                flexShrink: 1,
                                fontSize: 9,
                                position: 'absolute',
                                top: 5,
                                lineHeight: 16
                            }}>
                            {item?.ServiceName}
                        </Text>
                 
                    
                    </View>
                     
                    <View style={{ flex: 1 }}>
                        <Text style={{ ...FONTS?.h5, fontSize: 9 }}>
                            {I18n.t('quantity')}
                        </Text>
                        <View style={{ ...styles?.row }}>
                            <Text
                                style={{ ...styles?.price, fontSize: 9 }}>
                                {item?.qnt}
                            </Text>
                           
                        </View>
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={{ ...FONTS?.h5, fontSize: 9 }}>
                            {I18n.t('value')}
                        </Text>
                        <View style={{ ...styles?.row }}>
                            <Text
                                style={{ ...styles?.price, fontSize: 9 }}>
                                {item?.Price*item?.qnt}
                            </Text>
                            <Text
                                style={{ ...FONTS?.body6, fontSize: 9 }}
                            >
                                {I18n.t('rs')}
                            </Text>
                        </View>
                    </View>
                </View>
            )
        }

        return (
            <FlatList
                data={services}
                renderItem={renderItem}
                keyExtractor={item => `${item?.ServiceCode}`}
                showsVerticalScrollIndicator={false}

            />
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: COLORS.primary
            }}>
            <OverLoader
                isVisible={overLoader}
                Dismiss={(val) => {
                    setOverLoader(val)
                }}
            />
            <FeedBackMsg
                msg={feedBackMessage}
                isVisible={feedBackMessage != '' ? true : false}
                Dismiss={(val) => {
                    setFeedBackMessage(val == false ? '' : feedBackMessage)
                }}
            />
            {_Headers()}
            <View
                style={styles?.confirmView}
            >
                {_TabsView()}
                {tab == 'availableTime' ?
                    minday != '' ?
                     _calenderView() : 
                     null :
                    _orderView()}
            </View>
            <AppointmentsModal
                isVisiable={showAppointmentsModal}
                onDismiss={(val) => {
                    setShowAppointmentsModal(val)
                }}
                timeObj={time}
                saveOrder={(WorkTimeCode)=>{
                    saveOrder(WorkTimeCode, user, selectedDate, services).then(res => {
                        if (res?.ReturnData == null) {
                            setFeedBackMessage(I18nManager.isRTL ? res?.Message?.ContentAr : res?.Message?.ContentEn)

                        } else {
                            AsyncStorage.setItem('orderSaved', 'true')
                            navigation.navigate('Cart')
                        }

                    })
                }}
            />
            <MainFooter

                focused='order'
            />
        </SafeAreaView>
    )
}

export default ConfirmOrder;