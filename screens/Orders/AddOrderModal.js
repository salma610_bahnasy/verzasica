import React from "react";
import {
    Text,
    StyleSheet,
    TouchableOpacity,
    View,
    Image,
} from 'react-native';
import {
    COLORS,
    FONTS,
    icons,
    SIZES
} from "../../constants";
import Modal from "react-native-modal";
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from "@react-navigation/native";


const AddOrderModal = (props) => {
    const [show, setShow] = React.useState(false)
    const navigation = useNavigation()

    function _renderproducts() {
        return (
            <View style={{
                ...styles?.row,


            }}>
                <View style={{ flex: 1 }}>
                    <View style={[styles?.row]}>
                        <Image
                            style={{
                                ...styles?.xSmall,
                                tintColor: COLORS?.lightGreen
                            }}
                            source={icons?.done}
                        />
                        <Text
                            style={{
                                ...FONTS?.h4, marginHorizontal: 5
                            }}
                        >
                            نظافة كنب
                        </Text>
                    </View>
                    <Text
                        style={{
                            ...FONTS?.body5, textAlign: 'center'
                        }}
                    >في عربة التسوق</Text>
                </View>
                <View style={{ flex: 1 }}>
                    <View style={[styles?.row]}>

                        <Text
                            style={{
                                ...FONTS?.h5
                            }}
                        >
                            الكمية المطلوبه
                        </Text>
                    </View>
                    <Text
                        style={{
                            ...FONTS?.body5, textAlign: 'center'
                        }}
                    >1</Text>
                </View>
                <View style={{ flex: 1 }}>
                    <View style={[styles?.row]}>

                        <Text
                            style={{
                                ...FONTS?.h5
                            }}
                        >
                            مجموع العرية
                        </Text>
                    </View>
                    <Text
                        style={{
                            ...FONTS?.body5, textAlign: 'center'
                        }}
                    >
                        5000 ر.س
                    </Text>
                </View>


            </View>
        )
    }

    return (
        <View style={{ backgroundColor: '#000' }}>
            <Modal
                isVisible={props?.isVisible}
                useNativeDriver={true}
                onDismiss={() => {
                    setShow(false)
                    props?.Dismiss(false)
                }}
                onBackdropPress={() => {
                    setShow(false)
                    props?.Dismiss(false)
                }}
                animationIn='slideInUp'
                animationOut='slideOutDown'
                style={
                    styles.Modal
                }

            >
                <View style={{
                    width: '90%',
                    alignSelf: 'center',
                    backgroundColor: COLORS?.white,
                    height: 200,
                    justifyContent: 'space-between',
                    paddingHorizontal: SIZES?.Mpading,
                    paddingVertical: 40
                }}>
                    {_renderproducts()}
                    <View
                        style={{
                            ...styles?.row_space,
                        }}>


                        <TouchableOpacity
                            onPress={() => {
                                setShow(false)
                                props?.Dismiss(false)
                            }}
                            style={{
                                ...styles?.btn
                            }}>

                            <Text
                                style={{ ...FONTS?.body5 }}

                            >
                                إضافة خدمة اخري
                            </Text>
                        </TouchableOpacity>
                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            colors={['#536c68', '#21ba9f']}
                            style={{
                                ...styles?.btn
                            }}>

                            <TouchableOpacity
                                onPress={() => {
                                    props?.Dismiss(false)
                                    navigation.navigate('ConfirmOrder')
                                }}
                                style={{
                                    width: '100%', 
                                    alignContent:'center',
                                    alignItems:'center'
                                }}
                            >
                                <Text
                                    style={{
                                        ...FONTS?.body5,
                                        color: COLORS?.white
                                    }}
                                >
                                    إتمام الطلب
                            </Text>
                            </TouchableOpacity>
                        </LinearGradient>



                    </View>
                </View>
            </Modal>
        </View>
    )

}

export default AddOrderModal;

const styles = StyleSheet.create({

    Modal: {
        width: SIZES?.width,
        height: SIZES?.height,
        alignSelf: 'center',
        justifyContent: 'flex-start'

    },
    title: {
        ...FONTS?.h3,
        color: COLORS?.white
    },
    subtitle: {
        ...FONTS?.h4,
        color: COLORS?.white
    },
    row_space: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    xSmall: {
        width: 15,
        height: 15,
        resizeMode: 'contain'
    },
    btn: {
        borderWidth: 1,
        borderColor: COLORS?.primary,
        borderRadius: SIZES?.padding15,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        paddingHorizontal: SIZES?.padding15,
        flex: 1,
        marginHorizontal: SIZES?.Mpading,
        paddingVertical: SIZES.smallpading
    }
})