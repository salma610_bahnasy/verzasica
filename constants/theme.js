import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const COLORS = {
    // base colors
    primary: "#40b8a2",     // Green
    primary2: "#FBB344",    // Orange
    primary3: "#012548",    // Dark Blue
    secondary: "#FC2626",
    darkprimary: "#4a9789",
    tabColor:'#3cffba',
    // Red
    // colors
    black: "#1E1B26",
    white: "#FFFFFF",
    lightGray: "#64676D",
    lightGray2: "#EFEFF0",
    lightGray3: '#D4D5D6',
    lightGray4: '#7D7E84',
    gray: "#666666",
    gray1: "#282C35",
    darkRed: "#b32405",
    lightRed: "#C5505E",
    darkBlue: "#22273B",
    lightBlue: "#424BAF",
    darkGreen: "#213432",
    lightGreen: "#70e861",
    darkYellow: '#ffbe26',
    offWhite: '#ddd',
    primary_opacity: '#144f7973',
    secondary_opacity: '#03d0ff3b',
    lightWhite: '#e7e7e7',
    sharebg: '#eceef2'

};

export const SIZES = {
    // global sizes
    base: 8,
    font: 14,
    radius: 12,
    padding: 24,
    padding2: 36,
    smallpading: 5,
    Mpading: 10,
    padding20: 20,
    padding15: 15,

    padding3: 100,


    // font sizes
    largeTitle: 50,
    h1: 30,
    h2: 20,
    h3: 16,
    h4: 14,
    h5: 12,

    body1: 30,
    body2: 20,
    body3: 16,
    body4: 14,
    body5: 12,
    body6: 10,

    // app dimensions
    width,
    height
};

export const FONTS = {
    largeTitle: { fontFamily: "Cairo-regular", fontSize: SIZES.largeTitle, lineHeight: 55 },
    h1: { fontFamily: "Cairo-Black", fontSize: SIZES.h1, lineHeight: 36 },
    h2: { fontFamily: "Cairo-Bold", fontSize: SIZES.h2, lineHeight: 30 },
    h3: { fontFamily: "Cairo-Bold", fontSize: SIZES.h3, lineHeight: 22 },
    h4: { fontFamily: "Cairo-Bold", fontSize: SIZES.h4, lineHeight: 22 },
    h5: { fontFamily: "Cairo-SemiBold", fontSize: SIZES.h5, lineHeight: 22 },

    body1: { fontFamily: "Cairo-Regular", fontSize: SIZES.body1, lineHeight: 36 },
    body2: { fontFamily: "Cairo-Regular", fontSize: SIZES.body2, lineHeight: 30 },
    body3: { fontFamily: "Cairo-Regular", fontSize: SIZES.body3 },
    body4: { fontFamily: "Cairo-SemiBold", fontSize: SIZES.body4 },
    body5: { fontFamily: "Cairo-Regular", fontSize: SIZES.body5 },
    body6: { fontFamily: "Cairo-Regular", fontSize: SIZES.body6 },

};

const appTheme = { COLORS, SIZES, FONTS };

export default appTheme;