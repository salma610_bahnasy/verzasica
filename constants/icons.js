// tabs..................................
export const home = require("../assets/icons/007-home-1.png");
export const services = require("../assets/icons/006-lightning.png");
export const  cart = require("../assets/icons/005-shopping-bag.png");
export const user = require("../assets/icons/003-user.png");
export const downrow = require("../assets/icons/arrow-ic-20.png");
export const more_icon = require("../assets/icons/more_icon.png");
export const artraing = require("../assets/icons/artraing.png");
export const entraing = require("../assets/icons/entraing.png");
export const locationPin = require("../assets/icons/002-map-location.png");
export const downtraing = require("../assets/icons/001-sort-down.png");
export const orders = require("../assets/icons/008-shopping-cart-empty-side-view.png");
export const minus = require("../assets/icons/009-minus-sign.png");
export const plus_icon = require("../assets/icons/010-plus.png");
export const done = require("../assets/icons/done.png");
export const fav = require("../assets/icons/noun_favourite_-1.png");
export const addressIcon = require("../assets/icons/offers.png");
export const edit = require("../assets/icons/002-edit.png");
export const del=require('../assets/icons/001-delete.png');
//end tabs



export const card_icon = require("../assets/icons/card_icon.png");



export const claim_icon = require("../assets/icons/claim_icon.png");
export const dashboard_icon = require("../assets/icons/dashboard_icon.png");
export const menu_icon = require("../assets/icons/menu_icon.png");
export const notification_icon = require("../assets/icons/003-bell.png");
export const point_icon = require("../assets/icons/point_icon.png");
export const search_icon = require("../assets/icons/search_icon.png");
export const clock_icon = require("../assets/icons/clock_icon.png");
export const page_icon = require("../assets/icons/page_icon.png");
export const page_filled_icon = require("../assets/icons/page_filled_icon.png");
export const bookmark_icon = require("../assets/icons/bookmark_icon.png");
export const read_icon = require("../assets/icons/read_icon.png");
export const back_arrow_icon = require("../assets/icons/back_arrow_icon.png");
export const right_arrow_icon = require("../assets/icons/right1.png");



export const rightrow = require("../assets/icons/arrow-ic-56.png");
export const leftrow = require("../assets/icons/arrow-ic-19.png");

export const download = require("../assets/icons/download-ic-2.png");
export const share = require("../assets/icons/share-ic-3.png");
//settings
export const personalinfo = require("../assets/icons/id-ico-2.png");
export const permission = require("../assets/icons/security-ic.png");
export const safelogin = require("../assets/icons/security-ic-1.png");
export const settings = require("../assets/icons/setting-ic.png");
export const logout = require("../assets/icons/logout-ic.png");
//chat
export const Supervisor = require("../assets/icons/002-teacher-1.png");
export const school = require("../assets/icons/003-school.png");
export const sort = require("../assets/icons/sort.png");
export const sorted = require("../assets/icons/sorted.png");
export const attachment = require("../assets/icons/003-attachment-1.png");
export const camera = require("../assets/icons/001-camera.png");
export const microfon = require("../assets/icons/004-mic.png");
export const close = require("../assets/icons/001-close.png");
//arrows
export const left_arrow = require("../assets/icons/001-left-arrow.png");
export const right_arrow = require("../assets/icons/004-right-arrow-angle-1.png");
export const up_arrow = require("../assets/icons/001-up-chevron.png");
export const down_arrow = require("../assets/icons/001-down-chevron.png");
// 
export const front_of_bus = require("../assets/icons/001-front-of-bus.png");
export const autocall = require("../assets/icons/offers.png");
export const wallet = require("../assets/icons/001-wallet.png");
export const offers = require("../assets/icons/009-discounts.png");


export default {
    card_icon,
    claim_icon,
    dashboard_icon,
    menu_icon,
    notification_icon,
    point_icon,
    search_icon,
    clock_icon,
    page_icon,
    page_filled_icon,
    wallet,
    bookmark_icon,
    read_icon,
    back_arrow_icon,
    more_icon,

    downrow,
    leftrow,
    rightrow,
    download,
    share,
    personalinfo,
    permission,
    safelogin,
    settings,
    logout,
    Supervisor,
    school,
    sort,
    sorted,
    right_arrow_icon,
    attachment,
    camera,
    microfon,
    close,
    right_arrow,
    left_arrow,
    up_arrow,
    down_arrow,
    front_of_bus,
    autocall,
    del,
    // tabs
    services,
    home,
    cart,
    user,
    artraing,
    entraing,
    locationPin,
    downtraing,
    orders,
    minus,
    done,
    plus_icon,
    fav,
    addressIcon,
    edit,
    offers

}