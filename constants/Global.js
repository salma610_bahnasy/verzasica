import AsyncStorage from "@react-native-async-storage/async-storage";

const getCartItmsNum =()=>{
    return new Promise((resolve, reject) => {

        AsyncStorage.getItem('itemsInCart').then(res => {
            console.log(JSON.parse(res))
            if (res == null) {
                resolve([])
            } else {
                let data = JSON.parse(res)
                resolve(data)

            }
        })
    })

}

export{
    getCartItmsNum, 
}