
export const logo = require("../assets/images/logo.png");
export const splash = require("../assets/images/splash.png");
export const slider1 = require("../assets/images/Welcome.png")
export const slide2 = require("../assets/images/menu-drop-image2.png")
export const slide3 = require("../assets/images/slide3.png")
export const products = require("../assets/images/products.jpeg")


export default {
    slider1,
    slide2,
    slide3,
    logo,
    splash,
    products
}